/**
 * Created by zakpt on 2/8/2016.
 *
 * This file will hold all of the code relating to the routing
 * It is part of the RESTful API for the backend of the food truck tracker
 */

/**
 * @TODO Remove entity_sk from location
 */

module.exports = function(app, pg, conString, passport) {


    /**
     * Temporary strings to hold postgresql table creations.
     * Will be moved into appropriate functions later, this is just for code creation so all the tables can be
     * made without having the functions fully prototyped as group has not settled 100% on behavior
     */

    var menuItemTableString = "CREATE TABLE IF NOT EXISTS menuItem(item_sk serial primary key, item_name varchar(64)," +
        "item_description varchar(64), item_price float, item_picture bytea, is_vegan boolean, is_vegetarian boolean, menu_sk integer)";



    var adminTableString = "CREATE TABLE IF NOT EXISTS admin(admin_sk integer primary key, user_sk integer, company_sk" +
        "integer)";

    var userTableString = "CREATE TABLE IF NOT EXISTS user(user_sk integer primary key, username varchar(128), " +
        "password varchar(64), user_email varchar(64), user_rating integer, is_admin boolean)";

    /**
     * The appropriate procedure for updating the trucks table for when a user reports a new food truck will now be described.
     * We will assume the user is logged in to begin, which we will assume to mean that the user_sk value is also stored by the app
     * NOTE: exact details of the structures that http headers will need for certain endpoints will be detailed in the sections of code dedicated
     * to those endpoints
     *
     * When the sighting is made, a POST request should be made to the /location endpoint, to store the exact location details of the report.
     * When that endpoint returns, its http packet will contain the loc_sk of the location that was just uploaded, which will be necessary
     * for future tasks, so it should be stored in a varable
     * @TODO modify return so that the sk is returned
     * Once that is done, we will continue preparing to send a POST request to the /sighting endpoint, to store all of the information
     * about the user who submitted the report.  In order to do so, a GET request will have to be sent to the /trucks/:truckname
     * endpoint, where :truckname is the name of the truck reported.  The resulting JSON should be scanned to get the truck_sk key,
     * which will be necessary for the report.  If the truck_SK key is not found, a POST request will have to be made to the
     * /trucks endpoint to store information about the particular truck sighted.  Once this has been done, we will have all
     * the info needed to perform the POST request to /sighting
     */

    /**
     * The /trucks POST endpoint is used when a truck should be added to the trucks table.
     * The HTTP REQUEST body should have JSON of the following form, and be of the type 'application/json'
     * {
     *      "truckname": "TRUCKNAME STRING"
     *      "truckhours": "TRUCKHOURS STRING" (format to be determined)
     * }
     *
     * All fields in the REQUEST body must be non-null or the function will return an error
     * @return HTTP code 200 upon success, HTTP code 400 if any null values were encountered in the HTTP REQUEST
     */
    app.post('/trucks', function (req, res) {

        pg.connect(conString, function(err, client, done) {

            //handle the case where there is a connection error to the database
            if (err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {

                //If table for the trucks does not exist, create it
                //client.query("DROP TABLE IF EXISTS trucks");
                client.query("CREATE TABLE IF NOT EXISTS trucks(truck_sk serial primary key, truckname " +
                    "varchar(64), truckhours varchar(256), location_sk integer, company_sk integer, menu_sk integer," +
                    " number_of_times_reported integer, is_admin_controlled boolean)");

                var truckName = req.body.truckname;
                var truckHours = req.body.truckhours;

                //ensure truckName is provided, cannot create a new truck without a truckname
                if (truckName == null || truckHours == null) {
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot add new truck if all new truck fields are not specified" + "\n");
                    res.end();
                } else {

                    client.query("INSERT INTO trucks(truckname, number_of_times_reported, is_admin_controlled, truckhours) VALUES($1, $2, $3, $4)", [truckName, 0, 'true', truckHours], function (err, result) {
                        //calling done release the client back to the pool
                        console.log(result);

                        if (err) {
                            return console.error('Error running truckTableInsert', err);
                        }

                        done();
                        res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.write("New entry added to trucks" + "\n");
                        res.end();
                    });

                }
            }
        });
    });

    /**
     * The /trucks PUT endpoint is used to update an entry in the trucks table
     * The HTTP REQUEST body should have JSON of the following form
     * {
     *      "truck_sk": INTEGER
     *      "truckname": "STRING"
     *      "truckhours": "STRING representing the truck hours"
     *      "number_of_times_reported": INTEGER
     *      "is_admin_controlled": BOOLEAN
     * }
     * All values must be non-null or the function will not edit the desired truck
     * @return HTTP 200 if the update was successful, HTTP 400 if any null fields encountered in the HTTP BODY
     */
    app.put('/trucks', function (req, res) {

        pg.connect(conString, function(err, client, done) {

            //handle the case where there is a connection error to the database
            if(err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {

                var truckSK = req.body.truck_sk;
                var truckName = req.body.truckname;
                var truckHours = req.body.truckhours;
                var numTimesReported = req.body.number_of_times_reported;
                var isAdminControlled = req.body.is_admin_controlled;
                //console.log('STARTING QUERY');

                if (truckHours == null || truckName == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot update a truck if any fields are null" + "\n");
                    res.end();
                } else {

                    client.query('UPDATE trucks SET truckhours=($1), truckname=($2), number_of_times_reported=($3), is_admin_controlled=($4)' +
                        ' WHERE truck_sk=($5)', [truckHours, truckName, numTimesReported, isAdminControlled, truckSK], function (err, result) {
                        //calling done release the client back to the pool
                        if (err) {
                            return console.error('Error running truckTableUpdate\n', err);
                        }
                        done();

                        console.log(result);

                        res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.write("Entry Updated" + "\n");
                        res.end();
                    });
                }
            }
        });
    });

    /**
     * This route should be visited when the iOS or Android app would like a full list of trucks in the truck table
     * @return A string representation of the result from the database query
     */
    app.get('/trucks', function(req, res){

        pg.connect(conString, function(err, client, done) {
            var query = client.query("SELECT * FROM trucks", function (err, result){
                //calling done release the client back to the pool
                done();

                console.log(result);

                if (err){
                    return console.error('Error running truckTableGet', err);
                }

                //res.writeHead(200, {'Content-Type': 'text/plain'});
                //res.write(JSON.stringify(result.rows) + "\n");
                //res.end();
                res.status(200).json(result.rows);
            });
        });
    });

    /**
     * This function is used when a truckname is known and the corresponding database entry is needed
     * The ':/truckname' in the endpoint should correspond to the known truckname
     * @return A string representation of the JSON result of the get query
     * @TODO change return to actual JSON instead of string
     */
    app.get('/trucks/:truckname', function(req, res){

        var truckName = req.params.truckname;

        pg.connect(conString, function(err, client, done) {
            var query = client.query("SELECT * FROM trucks WHERE truckname=($1)",[truckName],  function (err, result){
                //calling done release the client back to the pool
                done();

                console.log(result);

                if (err){
                    return console.error('Error running truckTableGet', err);
                }

                res.status(200).json(result.rows);
            });
        });
    });


    /**
     * The /trucks DELETE endpoint should be used when an entry should be deleted from the trucks table
     * The HTTP REQUEST body should be of the following form:
     * {
     *      "truck_sk": INTEGER
     * }
     *
     * @return HTTP 200 if the delete was successful
     */
    app.delete('/trucks', function(req, res) {

        var truckSK = req.body.truck_sk;

        pg.connect(conString, function(err, client, done){

            client.query('DELETE FROM trucks WHERE truck_sk=($1)', [truckSK], function (err, result){
                //calling done release the client back to the pool
                done();

                console.log(result);

                if (err){
                    return console.error('Error running truckTableDelete', err);
                }

                res.writeHead(200, {'Content-Type': 'text/plain'});
                res.write("Entry Deleted" + "\n");
                res.end();
            });
        });
    });


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    app.post('/location', function (req, res) {
        pg.connect(conString, function(err, client, done) {

            //handle the case where there is a connection error to the database
            if(err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {
                //client.query("DROP TABLE IF EXISTS location");
                client.query("CREATE TABLE IF NOT EXISTS location(loc_sk serial primary key, loc_entity_sk integer," +
                    "loc_latitude float, loc_longitude float, loc_state varchar(2), loc_city varchar(64), loc_zip varchar(5), " +
                    "loc_address varchar(64), loc_country varchar(64))");

                var entitySK = req.body.loc_entity_sk;
                var latitude = req.body.loc_latitude;
                var longitude = req.body.loc_longitude;
                var state = req.body.loc_state;
                var city = req.body.loc_city;
                var zipcode = req.body.loc_zip;
                var address = req.body.loc_address;
                var country = req.body.loc_country;

                if (entitySK == null || latitude == null || longitude == null || state == null || city == null || zipcode == null || address == null || country == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot add new location if any fields are null" + "\n");
                    res.end();
                } else {

                    client.query("INSERT INTO location(loc_entity_sk, loc_latitude, loc_longitude, loc_state, loc_city, loc_zip, loc_address, loc_country)" +
                        " VALUES($1, $2, $3, $4, $5, $6, $7, $8)", [entitySK, latitude, longitude, state, city, zipcode, address, country], function (err, result) {

                        console.log(result);

                        if (err) {
                            return console.error('Error running locationTableInsert', err);
                        }

                        done();
                        res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.write("New entry added to location" + "\n");
                        res.end();
                    });
                }
            }
        });
    });

    app.get('/location', function (req, res) {
        pg.connect(conString, function(err, client, done) {
            var query = client.query("SELECT * FROM location", function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running locationTableGet', err);
                }
                console.log(result); //@TODO remove
                res.status(200).json(result.rows);
            });
        });
    });

    app.get('/location/:entitysk', function (req, res) {
        pg.connect(conString, function(err, client, done) {
            var entitySK = req.params.entitysk;

            var query = client.query("SELECT * FROM location WHERE loc_entity_sk=($1)", [entitySK], function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running locationTableGet', err);
                }

                res.status(200).json(result.rows);
            });
        });
    });

    app.put('/location', function (req, res) {
        pg.connect(conString, function(err, client, done) {

            //handle the case where there is a connection error to the database
            if (err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            }
            var locationSK = parseInt(req.body.loc_sk);
            var latitude = req.body.loc_latitude;
            var longitude = req.body.loc_longitude;
            var state = req.body.loc_state;
            var city = req.body.loc_city;
            var zipcode = req.body.loc_zip;
            var address = req.body.loc_address;
            var country = req.body.loc_country;
            var entitySK = req.body.loc_entity_sk;

            if (entitySK == null || latitude == null || longitude == null || state == null || city == null || zipcode == null || address == null || country == null) {
                res.writeHead(400, {'Content-Type': 'text/plain'});
                res.write("Cannot update a location if any fields are null" + "\n");
                res.end();
            } else {

                //client.query("INSERT INTO location(loc_truckname, loc_latitude, loc_longitude, loc_state, loc_city, loc_zip, loc_address, loc_country)" +
                //" VALUES($1, $2, $3, $4, $5, $6, $7, $8)", [truckName, latitude, longitude, state, city, zipcode, address, country], function (err, result){
                console.log("STARTING QUERY");
                client.query("UPDATE location SET loc_entity_sk=($1), loc_latitude=($2), loc_longitude=($3), loc_state=($4), loc_city=($5), loc_zip=($6), loc_address=($7), loc_country=($8)" +
                    " WHERE loc_sk=($9)", [entitySK, latitude, longitude, state, city, zipcode, address, country, locationSK], function (err, result) {

                    console.log(result);

                    if (err) {
                        return console.error('Error running locationTableInsert', err);
                    }

                    done();
                    res.writeHead(200, {'Content-Type': 'text/plain'});
                    res.write("New entry added to location" + "\n");
                    res.end();
                });
            }
        });
    });

    app.delete('/location', function (req, res) {
        var locationSK = req.body.loc_sk;

        pg.connect(conString, function(err, client, done){

            client.query('DELETE FROM location WHERE loc_sk=($1)', [locationSK], function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running locationTableDelete', err);
                }

                res.writeHead(200, {'Content-Type': 'text/plain'});
                res.write("Entry Deleted in Location" + "\n");
                res.end();
            });
        });
    });


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    app.post('/sighting', function (req, res) {
        pg.connect(conString, function(err, client, done) {

            //handle the case where there is a connection error to the database
            if(err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {
                client.query("CREATE TABLE IF NOT EXISTS sighting(sight_sk serial primary key, sight_rating integer," +
                    "sight_description varchar(256), sight_time timestamp, user_sk integer, truck_sk integer, loc_sk integer)");

                var sightRating = req.body.sight_rating;
                var sightDescription = req.body.sight_description;
                var sightTime = req.body.sight_time;
                var userSK = req.body.user_sk;
                var truckSK = req.body.truck_sk;
                var locSK = req.body.loc_sk;

                if (sightRating == null || sightDescription == null || sightTime == null || userSK == null || truckSK == null || locSK == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot add new sighting if any fields are null" + "\n");
                    res.end();
                } else {

                    client.query("INSERT INTO sighting(sight_rating, sight_description, sight_time, user_sk, truck_sk, loc_sk)" +
                        " VALUES($1, $2, $3, $4, $5, $6)", [sightRating, sightDescription, sightTime, userSK, truckSK, locSK], function (err, result) {

                        console.log(result);

                        if (err) {
                            return console.error('Error running sightingTableInsert', err);
                        }

                        done();
                        res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.write("New entry added to sighting" + "\n");
                        res.end();
                    });
                }
            }
        });
    });

    app.get('/sighting', function (req, res) {
        pg.connect(conString, function(err, client, done) {
            var query = client.query("SELECT * FROM sighting", function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running sightingTableGet', err);
                }

                res.status(200).json(result.rows);
            });
        });
    });

    app.get('/sighting/:trucksk', function (req, res) {
        var truckSK = req.params.trucksk;
        pg.connect(conString, function(err, client, done) {
            var query = client.query("SELECT * FROM sighting WHERE truck_sk=($1)", [truckSK], function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running sightingTableGet', err);
                }

                res.status(200).json(result.rows);
            });
        });
    });

    app.put('/sighting', function (req, res) {
        pg.connect(conString, function(err, client, done) {

            //handle the case where there is a connection error to the database
            if(err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {

                var sightSK = req.body.sight_sk;
                var sightRating = req.body.sight_rating;
                var sightDescription = req.body.sight_description;
                var sightTime = req.body.sight_time;
                var userSK = req.body.user_sk;
                var truckSK = req.body.truck_sk;
                var locSK = req.body.loc_sk;

                if (sightRating == null || sightDescription == null || sightTime == null || userSK == null || truckSK == null || locSK == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot update sighting if any fields are null" + "\n");
                    res.end();
                } else {

                    client.query("UPDATE sighting SET sight_rating=($1), sight_description=($2), sight_time=($3), user_sk=($4), truck_sk=($5), loc_sk=($6)" +
                        " WHERE sight_sk=($7)", [sightRating, sightDescription, sightTime, userSK, truckSK, locSK, sightSK], function (err, result) {

                        console.log(result);

                        if (err) {
                            return console.error('Error running sightingTableUpdate', err);
                        }

                        done();
                        res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.write("Entry updated in sighting" + "\n");
                        res.end();
                    });
                }
            }
        });
    });

    app.delete('/sighting', function (req, res) {
        var sightSK = req.body.sight_sk;

        pg.connect(conString, function(err, client, done){

            client.query('DELETE FROM sighting WHERE sight_sk=($1)', [sightSK], function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running locationTableDelete', err);
                }

                res.writeHead(200, {'Content-Type': 'text/plain'});
                res.write("Entry Deleted in Sighting" + "\n");
                res.end();
            });
        });
    });

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    app.post('/company', function (req, res) {
        pg.connect(conString, function(err, client, done) {

            //handle the case where there is a connection error to the database
            if(err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {
                client.query("CREATE TABLE IF NOT EXISTS company(company_sk serial primary key, " +
                    "company_name varchar(64), company_phone varchar(16), company_location_sk integer)");

                var companyName = req.body.company_name;
                var companyPhone = req.body.company_phone;
                var companyLocationSK = req.body.company_location_sk;

                if (companyName == null || companyPhone == null || companyLocationSK == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot add new company if any fields are null" + "\n");
                    res.end();
                } else {

                    client.query("INSERT INTO company(company_name, company_phone, company_location_sk)" +
                        " VALUES($1, $2, $3)", [companyName, companyPhone, companyLocationSK], function (err, result) {

                        console.log(result);

                        if (err) {
                            return console.error('Error running companyTableInsert', err);
                        }

                        done();
                        res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.write("New entry added to company" + "\n");
                        res.end();
                    });
                }
            }
        });
    });

    app.get('/company', function (req, res) {
        pg.connect(conString, function(err, client, done) {
            var query = client.query("SELECT * FROM company", function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running companyTableGet', err);
                }

                res.status(200).json(result.rows);
            });
        });
    });

    app.get('/company/:companyname', function (req, res) {
        pg.connect(conString, function(err, client, done) {
            var companyName = req.params.companyname;
            var query = client.query("SELECT * FROM company WHERE company_name=($1)", [companyName], function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running companyTableGet', err);
                }

                res.status(200).json(result.rows);
            });
        });
    });


    app.put('/company', function (req, res) {
        pg.connect(conString, function(err, client, done) {

            //handle the case where there is a connection error to the database
            if(err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {

                var companySK = req.body.company_sk;
                var companyName = req.body.company_name;
                var companyPhone = req.body.company_phone;
                var companyLocationSK = req.body.company_location_sk;

                if (companyName == null || companyPhone == null || companyLocationSK == null || companySK == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot edit company if any fields are null" + "\n");
                    res.end();
                } else {

                    client.query("UPDATE company SET company_name=($1), company_phone=($2), company_location_sk=($3))" +
                    " WHERE company_sk=($4)", [companyName, companyPhone, companyLocationSK, companySK], function (err, result) {

                        console.log(result);

                        if (err) {
                            return console.error('Error running companyTableUpdate', err);
                        }

                        done();
                        res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.write("Entry updated in company" + "\n");
                        res.end();
                    });
                }
            }
        });
    });

    app.delete('/company', function (req, res) {
        var companySK = req.body.company_sk;

        pg.connect(conString, function(err, client, done){

            client.query('DELETE FROM company WHERE company_sk=($1)', [companySK], function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running companyTableDelete', err);
                }

                res.writeHead(200, {'Content-Type': 'text/plain'});
                res.write("Entry Deleted in company" + "\n");
                res.end();
            });
        });
    });

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    app.post('/menu', function (req, res) {
        pg.connect(conString, function(err, client, done) {

            //handle the case where there is a connection error to the database
            if(err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {
                client.query("CREATE TABLE IF NOT EXISTS menu(menu_sk serial primary key, menu_name varchar(64), menu_picture bytea)");

                var menuName = req.body.menu_name;
                var menuPicture = req.body.menu_picture;

                if (menuName == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot add new menu if menu name is null" + "\n");
                    res.end();
                } else {

                    client.query("INSERT INTO menu(menu_name, menu_picture) VALUES($1, $2)", [menuName, menuPicture], function (err, result) {

                        console.log(result);

                        if (err) {
                            return console.error('Error running menuTableInsert', err);
                        }

                        done();
                        res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.write("New entry added to menu" + "\n");
                        res.end();
                    });
                }
            }
        });
    });

    app.get('/menu', function (req, res) {
        pg.connect(conString, function(err, client, done) {
            var query = client.query("SELECT * FROM menu", function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running menuTableGet', err);
                }

                res.status(200).json(result.rows);
            });
        });
    });

    app.get('/menu/:menuname', function (req, res) {
        pg.connect(conString, function(err, client, done) {
            var menuName = req.params.menuname;
            var query = client.query("SELECT * FROM menu WHERE menu_name=($1)", [menuName], function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running menuTableGet', err);
                }

                res.status(200).json(result.rows);
            });
        });
    });

    app.put('/menu', function (req, res) {
        pg.connect(conString, function(err, client, done) {

            //handle the case where there is a connection error to the database
            if(err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {

                var menuSK = req.body.menu_sk;
                var menuName = req.body.menu_name;
                var menuPicture = req.body.menu_picture;

                if (menuSK == null || menuName == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot edit menu if menu_sk or menu_name are null" + "\n");
                    res.end();
                } else {

                    client.query("UPDATE menu SET menu_name=($1), menu_picture=($2))" +
                        " WHERE menu_sk=($3)", [menuName, menuPicture, menuSK], function (err, result) {

                        console.log(result);

                        if (err) {
                            return console.error('Error running menuTableUpdate', err);
                        }

                        done();
                        res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.write("Entry updated in menu" + "\n");
                        res.end();
                    });
                }
            }
        });
    });

    app.delete('/menu', function (req, res) {
        var menuSK = req.body.menu_sk;

        pg.connect(conString, function(err, client, done){

            client.query('DELETE FROM menu WHERE menu_sk=($1)', [menuSK], function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running menuTableDelete', err);
                }

                res.writeHead(200, {'Content-Type': 'text/plain'});
                res.write("Entry Deleted in menu" + "\n");
                res.end();
            });
        });
    });


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    app.post('/admin', function (req, res) {
        pg.connect(conString, function(err, client, done) {
            //handle the case where there is a connection error to the database
            if(err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {
                client.query("CREATE TABLE IF NOT EXISTS admin(admin_sk serial primary key, user_sk integer, company_sk integer)");

                var userSK = req.body.user_sk;
                var companySK = req.body.company_sk;

                if (userSK == null || companySK == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot add new admin if fields are null" + "\n");
                    res.end();
                } else {

                    client.query("INSERT INTO admin(user_sk, company_sk) VALUES($1, $2)", [userSK, companySK], function (err, result) {

                        console.log(result);

                        if (err) {
                            return console.error('Error running adminTableInsert', err);
                        }

                        done();
                        res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.write("New entry added to admin" + "\n");
                        res.end();
                    });
                }
            }
        });
    });

    app.get('/admin', function (req, res) {
        pg.connect(conString, function(err, client, done) {
            var query = client.query("SELECT * FROM admin", function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running adminTableGet', err);
                }

                res.status(200).json(result.rows);
            });
        });
    });


    app.put('/admin', function (req, res) {
        pg.connect(conString, function(err, client, done) {

            //handle the case where there is a connection error to the database
            if(err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {

                var adminSK = req.body.admin_sk;
                var userSK = req.body.user_sk;
                var companySK = req.body.company_sk;

                if (adminSK == null || userSK == null || companySK == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot edit admin if any fields are null" + "\n");
                    res.end();
                } else {

                    client.query("UPDATE admin SET user_sk=($1), company_sk=($2)" +
                        " WHERE admin_sk=($3)", [userSK, companySK, adminSK], function (err, result) {

                        console.log(result);

                        if (err) {
                            return console.error('Error running adminTableUpdate', err);
                        }

                        done();
                        res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.write("Entry updated in admin" + "\n");
                        res.end();
                    });
                }
            }
        });
    });

    app.delete('/admin', function (req, res) {
        var adminSK = req.body.admin_sk;

        pg.connect(conString, function(err, client, done){

            client.query('DELETE FROM admin WHERE admin_sk=($1)', [adminSK], function (err, result){
                //calling done release the client back to the pool
                done();

                if (err){
                    return console.error('Error running adminTableDelete', err);
                }

                res.writeHead(200, {'Content-Type': 'text/plain'});
                res.write("Entry Deleted in admin" + "\n");
                res.end();
            });
        });
    });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * These routes will be convenience routes that are not strictly part of the API
     */


    /**
     * This route is a convenience route for easy reporting of a truck sighting
     */
    app.post('/reporttrucksighting', passport.authenticate('basic', { session: false }), function(req, res) {

        pg.connect(conString, passport.authenticate('basic', {session: false}), function (err, client, done){

            if(err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {
                //truck portion
                var truckName = req.body.truckname;
                var truckHours = req.body.truckhours;
                var truckSK = -1;

                //ensure truckName is provided, cannot create a new truck without a truckname
                if (truckName == null) {
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot add new truck if all new truck fields are not specified" + "\n");
                    res.end();
                    return;
                } else {
                    var queryResult = null;
                    console.log("first select");
                    //check if truck is already in table
                    client.query("SELECT * FROM trucks WHERE truckname=($1)",[truckName],  function (err, result){

                        console.log(result);

                        if (err){
                            return console.error('Error running truckTableGet', err);
                        }

                        queryResult = result.rows;

                        console.log("time to check query result");
                        //if truck not in table
                        if (result.rowCount == 0) {
                            console.log("QUERYRESULT IS NULL");

                            client.query("INSERT INTO trucks(truckname, number_of_times_reported, is_admin_controlled, truckhours) VALUES($1, $2, $3, $4)", [truckName, 0, 'false', truckHours], function (err, result) {
                                console.log(result);

                                if (err) {
                                    return console.error('Error running truckTableInsert', err);
                                }
                                console.log("INSERTED");

                                client.query("SELECT * FROM trucks WHERE truckname=($1)",[truckName],  function (err, result){
                                    console.log(result);
                                    queryResult = result.rows;

                                    if (err) {
                                        return console.error('Error running truckTableGet', err);
                                    }
                                    console.log("GOT NEW SK");
                                    console.log(queryResult);
                                    truckSK = queryResult[0]["truck_sk"];
                                    var numberOfTimesReported = queryResult[0]["number_of_times_reported"];
                                    var isAdminControlled = queryResult[0]["is_admin_controlled"];
                                    numberOfTimesReported += 1;

                                    client.query('UPDATE trucks SET truckhours=($1), truckname=($2), number_of_times_reported=($3), is_admin_controlled=($4)' +
                                        ' WHERE truck_sk=($5)', [truckHours, truckName, numberOfTimesReported, isAdminControlled, truckSK], function (err, result) {
                                        //calling done release the client back to the pool
                                        if (err) {
                                            return console.error('Error running truckTableUpdate\n', err);
                                        }

                                        console.log(result);

                                        partTwo(req, res, client, truckSK, done);

                                    });
                                });
                            });
                        } else {
                            console.log("GOT NEW SK");
                            console.log(queryResult);
                            truckSK = queryResult[0]["truck_sk"];
                            var numberOfTimesReported = queryResult[0]["number_of_times_reported"];
                            var isAdminControlled = queryResult[0]["is_admin_controlled"];
                            numberOfTimesReported += 1;

                            client.query('UPDATE trucks SET truckhours=($1), truckname=($2), number_of_times_reported=($3), is_admin_controlled=($4)' +
                                ' WHERE truck_sk=($5)', [truckHours, truckName, numberOfTimesReported, isAdminControlled, truckSK], function (err, result) {
                                //calling done release the client back to the pool
                                if (err) {
                                    return console.error('Error running truckTableUpdate\n', err);
                                }

                                console.log(result);
                                partTwo(req, res, client, truckSK, done);
                            });
                        }
                    });
                }
            }
        });
    });

    function partTwo(req, res, client, truckSK, done){
        //location portion
        client.query("CREATE TABLE IF NOT EXISTS location(loc_sk serial primary key, loc_entity_sk integer," +
            "loc_latitude float, loc_longitude float, loc_state varchar(2), loc_city varchar(64), loc_zip varchar(5), " +
            "loc_address varchar(64), loc_country varchar(64))");

        console.log("STARTING LOCATIION");
        var entitySK = truckSK;
        var latitude = req.body.loc_latitude;
        var longitude = req.body.loc_longitude;
        var state = req.body.loc_state;
        var city = req.body.loc_city;
        var zipcode = req.body.loc_zip;
        var address = req.body.loc_address;
        var country = req.body.loc_country;

        var queryResult2 = null;

        if (latitude == null || longitude == null || state == null || city == null || zipcode == null || address == null || country == null){
            res.writeHead(400, {'Content-Type': 'text/plain'});
            res.write("Cannot add new location if any fields are null" + "\n");
            res.end();
            return;
        } else {
            console.log("STARTING SELECT LOC");
            client.query("SELECT * FROM location WHERE loc_latitude=($1) AND loc_longitude=($2)", [latitude, longitude], function (err, result){

                console.log("LOC SELECT SUCCESS");
                if (err){
                    return console.error('Error running locationTableGet', err);
                }

                queryResult2 = result.rows;
                if (result.rowCount == 0) {
                    client.query("INSERT INTO location(loc_entity_sk, loc_latitude, loc_longitude, loc_state, loc_city, loc_zip, loc_address, loc_country)" +
                        " VALUES($1, $2, $3, $4, $5, $6, $7, $8)", [entitySK, latitude, longitude, state, city, zipcode, address, country], function (err, result) {

                        console.log(result);

                        if (err) {
                            return console.error('Error running locationTableInsert', err);
                        }

                        client.query("SELECT * FROM location WHERE loc_latitude=($1) AND loc_longitude=($2)", [latitude, longitude], function (err, result){
                            var queryResult3 = result.rows;
                            var locationSK = queryResult3[0]["loc_sk"];
                            partThree(req, res, client, locationSK, truckSK, done);
                        });


                    });
                } else {
                    var locationSK = queryResult2[0]["loc_sk"];
                    client.query("UPDATE location SET loc_entity_sk=($1), loc_latitude=($2), loc_longitude=($3), loc_state=($4), loc_city=($5), loc_zip=($6), loc_address=($7), loc_country=($8)" +
                        " WHERE loc_sk=($9)", [entitySK, latitude, longitude, state, city, zipcode, address, country, locationSK], function (err, result) {

                        console.log(result);

                        if (err) {
                            return console.error('Error running locationTableInsert', err);
                        }

                        partThree(req, res, client, locationSK, truckSK, done);

                    });
                }
            });
        }

    }

    function partThree(req, res, client, locationSK, truckSK, done){
        //sighting portion
        client.query("CREATE TABLE IF NOT EXISTS sighting(sight_sk serial primary key, sight_rating integer," +
            "sight_description varchar(256), sight_time timestamp, user_sk integer, truck_sk integer, loc_sk integer)");

        var sightRating = req.body.sight_rating;
        var sightDescription = req.body.sight_description;
        var sightTime = req.body.sight_time;
        var userSK = req.body.user_sk;
        var locSK = locationSK;

        if (sightRating == null || sightDescription == null || sightTime == null || userSK == null || truckSK == null || locSK == null){
            res.writeHead(400, {'Content-Type': 'text/plain'});
            res.write("Cannot add new sighting if any fields are null" + "\n");
            res.end();
        } else {

            client.query("INSERT INTO sighting(sight_rating, sight_description, sight_time, user_sk, truck_sk, loc_sk)" +
                " VALUES($1, $2, $3, $4, $5, $6)", [sightRating, sightDescription, sightTime, userSK, truckSK, locSK], function (err, result) {

                console.log(result);

                if (err) {
                    return console.error('Error running sightingTableInsert', err);
                }

                done();
                res.writeHead(200, {'Content-Type': 'text/plain'});
                res.write("New truck sighting reported, entries added or updated in all three tables" + "\n");
                res.end();
            });
        }
    }

    app.post("/getnearesttruckslocations", function(req, res){

        console.log("IN trucks locations get");

        var myLongitude = req.body.loc_longitude;
        var myLatitude = req.body.loc_latitude;

        pg.connect(conString, function(err, client, done){
            client.query("SELECT * FROM location WHERE loc_latitude<($1) AND loc_latitude>($2) AND loc_longitude<($3)" +
                "AND loc_longitude>($4)", [myLatitude +.05, myLatitude -.05, myLongitude +.05, myLongitude -.05], function (err, result) {

                if (err) {
                    return console.error('Error running companyTableGet', err);
                }

                //console.log(result);

                var truckList = [];

                var myResults = result;

                client.query("SELECT * FROM trucks",  function (err, result){

                    if (err){
                        return console.error('Error running truckTableGet', err);
                    }

                    for(var i=0; i<result.rows.length; i++){
                        for(var j =0; j<myResults.rows.length; j++){
                            if (result.rows[i]["truck_sk"] == myResults.rows[j]["loc_entity_sk"]){
                                truckList.push(result.rows[i]);
                            }
                        }
                    }

                    var toReturn = [];
                    toReturn.push(myResults.rows);
                    toReturn.push(truckList);


                    //calling done release the client back to the pool
                    done();

                    res.status(200).json(toReturn);
                });


            });
        });
    });

    app.get("/resettrucks", function (req, res){

        pg.connect(conString, function (err, client, done) {
            client.query("DROP TABLE trucks", function (err, result) {
                client.query("CREATE TABLE IF NOT EXISTS trucks(truck_sk serial primary key, truckname " +
                    "varchar(64), truckhours varchar(256), location_sk integer, company_sk integer, menu_sk integer," +
                    " number_of_times_reported integer, is_admin_controlled boolean)", function (err, result){
                    //calling done release the client back to the pool
                    done();

                    if (err){
                        return console.error('Error running menuTableDelete', err);
                    }

                    res.status(200).json([]);
                });
            });
        });
    });

    app.get("/resetlocation", function (req, res) {
        pg.connect(conString, function (err, client, done) {
            client.query("DROP TABLE location", function (err, result) {
                client.query("CREATE TABLE IF NOT EXISTS location(loc_sk serial primary key, loc_entity_sk integer," +
                    "loc_latitude float, loc_longitude float, loc_state varchar(2), loc_city varchar(64), loc_zip varchar(5), " +
                    "loc_address varchar(64), loc_country varchar(64))", function (err, result){
                    //calling done release the client back to the pool
                    done();

                    if (err){
                        return console.error('Error running menuTableDelete', err);
                    }

                    res.status(200).json([]);
                });
            });
        });
    });

    app.get("/resetsighting", function (req, res) {
        pg.connect(conString, function (err, client, done) {
            client.query("DROP TABLE sighting", function (err, result) {
                client.query("CREATE TABLE IF NOT EXISTS sighting(sight_sk serial primary key, sight_rating integer," +
                    "sight_description varchar(256), sight_time timestamp, user_sk integer, truck_sk integer, loc_sk integer)",function (err, result) {
                    //calling done release the client back to the pool
                    done();

                    if (err) {
                        return console.error('Error running menuTableDelete', err);
                    }

                    res.status(200).json([]);
                });
            });
        });
    });

    app.get("/resetusers", function (req, res) {
        console.log("In reset users");
        pg.connect(conString, function (err, client, done) {
            client.query("DROP TABLE users", function (err, result) {
                if (err) {
                    return console.error('Error running menuTableDelete', err);
                }
                console.log(result);
                client.query("CREATE TABLE IF NOT EXISTS users(user_sk serial primary key, username varchar(128), password varchar(64), user_email varchar(64), user_rating integer, is_admin boolean)", function(err, result){

                    //calling done release the client back to the pool
                    done();

                    if (err) {
                        return console.error('Error running menuTableDelete', err);
                    }
                    console.log(result);
                    res.status(200).json([]);
                });
            });
        });
    });


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Login stuff


    app.post("/users", function(req, res){
        var username = req.body.username;
        var password = req.body.password;
        var email = req.body.user_email;
        var rating = 1
        var isAdmin = 'false';

        var response = res;
        if (username == null || password == null){
            console.log("Username or Pass null");
            return;
        }

        pg.connect(conString, function(err, client, done) {
            client.query("CREATE TABLE IF NOT EXISTS users(user_sk serial primary key, username varchar(128), " + "password varchar(64), user_email varchar(64), user_rating integer, is_admin boolean)", function(req, res){

                client.query("SELECT * FROM users WHERE username=($1)", [username], function(err, result) {

                    //should never be true, if so it means two people have the same facebook ID
                    //should always fall through to else
                    if (result == null) {
                        console.log("Duplicate facebook token");
                    } else {
                        client.query("INSERT INTO users(username, password, user_email, user_rating, is_admin) VALUES ($1, $2, $3, $4, $5)", [username, password, email, rating, isAdmin], function (err, result) {
                            if (err) {
                                console.log(err);
                            }

                            done();
                            response.writeHead(200, {'Content-Type': 'text/plain'});
                            response.write("New User Added to Database" + "\n");
                            response.end();
                        });
                    }
                });
            });
        });
    });


    app.get("/users/:username", function(req, res){
        var username = req.params.username;

        pg.connect(conString, function(err, client, done) {
            var query = client.query("SELECT * FROM users WHERE username($1)",[username],  function (err, result){
                //calling done release the client back to the pool
                done();

                console.log(result);

                if (err){
                    return console.error('Error running userGet', err);
                }

                res.status(200).json(result.rows);
            });
        });
    });

    app.get("/users", function(req, res){
        pg.connect(conString, function(err, client, done) {
            var query = client.query("SELECT * FROM users",  function (err, result){
                //calling done release the client back to the pool
                done();

                console.log(result);

                if (err){
                    return console.error('Error running userGet', err);
                }

                res.status(200).json(result.rows);
            });
        });
    });

    app.get('/api/me', passport.authenticate('basic', { session: false }),
        function(req, res) {
            res.json(req.user);
    });

    app.post("/newcompany", passport.authenticate('basic', { session: false }), function(req, res){
        pg.connect(conString, function(err, client, done) {

            //handle the case where there is a connection error to the database
            if(err) {
                return console.error('error fetching client from pool', err);

                //if we reach this else, the server was successfully connected to
            } else {
                //client.query("DROP TABLE IF EXISTS location");
                client.query("CREATE TABLE IF NOT EXISTS location(loc_sk serial primary key, loc_entity_sk integer," +
                    "loc_latitude float, loc_longitude float, loc_state varchar(2), loc_city varchar(64), loc_zip varchar(5), " +
                    "loc_address varchar(64), loc_country varchar(64))");

                var entitySK = req.body.loc_entity_sk;
                var latitude = req.body.loc_latitude;
                var longitude = req.body.loc_longitude;
                var state = req.body.loc_state;
                var city = req.body.loc_city;
                var zipcode = req.body.loc_zip;
                var address = req.body.loc_address;
                var country = req.body.loc_country;
                var companyName = req.body.company_name;
                var companyPhone = req.body.company_phone;
                var companyLocationSK = null;
                entitySK = -1;

                if (companyName == null || companyPhone == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot add new company if any fields are null" + "\n");
                    res.end();
                } else if (entitySK == null || latitude == null || longitude == null || state == null || city == null || zipcode == null || address == null || country == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot add new location if any fields are null" + "\n");
                    res.end();
                } else {

                    client.query("INSERT INTO location(loc_entity_sk, loc_latitude, loc_longitude, loc_state, loc_city, loc_zip, loc_address, loc_country)" +
                        " VALUES($1, $2, $3, $4, $5, $6, $7, $8)", [entitySK, latitude, longitude, state, city, zipcode, address, country], function (err, result) {

                        console.log(result);

                        if (err) {
                            return console.error('Error running locationTableInsert', err);
                        }

                        client.query("SELECT loc_sk FROM location WHERE loc_latitude=($1) AND loc_longitude=($2)", [latitude, longitude], function (err, result) {
                            companyLocationSK = result.rows[0].loc_sk;
                            client.query("CREATE TABLE IF NOT EXISTS company(company_sk serial primary key, " +
                                "company_name varchar(64), company_phone varchar(16), company_location_sk integer)", function (result, err) {


                                client.query("INSERT INTO company(company_name, company_phone, company_location_sk)" +
                                    " VALUES($1, $2, $3)", [companyName, companyPhone, companyLocationSK], function (err, result) {

                                    console.log(result);

                                    if (err) {
                                        return console.error('Error running companyTableInsert', err);
                                    }

                                    done();
                                    res.writeHead(200, {'Content-Type': 'text/plain'});
                                    res.write("New entry added to company" + "\n");
                                    res.end();
                                });
                            });
                        });
                    });
                }
            }
        });
    });


    app.post("/maketruckadmincontrolled", passport.authenticate('basic', { session: false }), function(req, res){
        var truckName = req.body.truckname;

        pg.connect(conString, function(err, client, done) {
            var query = client.query("SELECT * FROM trucks WHERE truckname=($1)",[truckName],  function (err, result){

                console.log(result);

                if (err){
                    return console.error('Error running truckTableGet', err);
                }
                if (result.rowCount == 0){
                    res.status(400).json({});
                }

                var truckSK = result.rows[0].truck_sk;
                var companySK = req.body.company_sk;

                //console.log('STARTING QUERY');

                if (truckHours == null || truckName == null){
                    res.writeHead(400, {'Content-Type': 'text/plain'});
                    res.write("Cannot update a truck if any fields are null" + "\n");
                    res.end();
                } else {

                    client.query('UPDATE trucks SET company_sk=($1), is_admin_controlled=($2)' +
                        ' WHERE truck_sk=($3)', [companySK, 'true', truckSK], function (err, result) {
                        //calling done release the client back to the pool
                        if (err) {
                            return console.error('Error running truckTableUpdate\n', err);
                        }
                        done();

                        console.log(result);

                        res.writeHead(200, {'Content-Type': 'text/plain'});
                        res.write("Entry Updated" + "\n");
                        res.end();
                    });
                }
            });
        });
    });
};