var strategy = require('passport-http').BasicStrategy;


module.exports = function(passport, pg, conString) {

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    /**
     * BASIC LOGIN
     * @TODO add password encryption
     */

    passport.use('basic', new strategy({
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, callback) {
            pg.connect(conString, function(err, client, done) {
                client.query("SELECT * FROM users WHERE username=($1)", [username], function (err, result) {
                    done();
                    console.log(result);
                    var user = result.rows[0];
                    if (err)
                        return callback(err);
                    if (result.rowCount == 0)
                        return callback(null, false);
                    if (result.rows[0].is_admin == 'false')
                        return callback(null, user);
                    if (result.rows[0].password !== password){
                        return callback(null, false);
                    }
                    //login success
                    return callback(null, {username: user.username, user_email: user.user_email});
                });
            });
        }));
};