/**
 * Created by zakpt on 2/8/2016.
 *
 * This index file serves as the main file for the backend.
 * The backend will be a RESTFUL service that is intended to be deployed on Heroku
 * This file will mainly consist of the code required to include all of the node modules that will be required to run,
 * the setup for said modules, and require statements to include all of the code for the other operations of the server.
 */

/**
 * Setup Express
 */
var express = require('express');
var app = express();
var passport =require('passport');

/**
 * Setup body-parser
 */
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/**
 * Setup PG (PostgresSQL module)
 */
var pg = require('pg').native;
//@TODO fill in conString with server info once Heroku is setup
//var conString = "postgres://username:password@localhost/database";
var conString = "postgres://qurfwabumwazkp:FJoEfaTvzmI4uw0ed1ED2khAmG@ec2-107-20-136-89.compute-1.amazonaws.com:5432/d98f7rhm42amsl";

/**
 * Initialize passport
 */
app.use(passport.initialize());

/**
 * Require the passport strategy
 */
require('./app/passport.js')(passport, pg, conString);

/**
 * Require the routes file for the server
 * @param app: the initialized express app
 * @param pg: the initialized pg (postgresql) app
 */
require('./app/routes.js')(app, pg, conString, passport);


/**
 * Have the server begin listen to begin serving requests
 * @TODO Need to change port number to parametrize it for Heroku
 */
app.listen(process.env.PORT || 8080, function () {
    console.log('Example app listening on port 8080!');
});