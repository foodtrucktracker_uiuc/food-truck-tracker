package com.ftt.trucktracker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
/*
Main menu for the app.  Is responsible for getting the user's current location and updating truck locations based on that.
Map is based on the Google Maps API.
Also serves as the hub from which to transition between the other screens.
 */
public class MainActivity extends Activity
        implements ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.

    protected static final String TAG = "basic-location-sample";
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    GoogleMap map;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest loc_req;
    protected Location loc;
    protected double lat, lng;
    protected CameraUpdate center;
    protected CameraUpdate zoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        buildGoogleApiClient();

    }

    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    protected void createLocationRequest() {
        loc_req = new LocationRequest();

        loc_req.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        loc_req.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        loc_req.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, loc_req, this);
    }

    protected void stopLocationUpdates() {

        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }


    private void update_lat_long() {
        if (loc != null) {
            lat = loc.getLatitude();
            lng = loc.getLongitude();

        }

        map.clear();

        //Center Camera on user position and set zoom level
        center = CameraUpdateFactory.newLatLng((new LatLng(lat, lng)));
        zoom = CameraUpdateFactory.zoomTo(15);
        map.moveCamera(center);
        map.animateCamera(zoom);

        //Add marker for user location
        MarkerOptions mp = new MarkerOptions();
        mp.position(new LatLng(lat, lng));
        mp.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        map.addMarker(mp);

        // Get nearby truck data from server
        JSONArray jsonArray = getTruckData(lat, lng);
        JSONArray jsonArray_loc = null;
        JSONArray jsonArray_trucks = null;

        if (jsonArray != null) {


            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    JSONObject loc = jsonObject.getJSONObject("location");
                    JSONObject truck = jsonObject.getJSONObject("truck");

                    double truck_lat = loc.getDouble("loc_latitude");
                    double truck_lng = loc.getDouble("loc_longitude");
                    String truck_name = truck.getString("truckname");
                    map.addMarker(new MarkerOptions().position(new LatLng(truck_lat, truck_lng)).title(truck_name));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public JSONArray getTruckData(double lat, double lng) {
        URL url = null;
        HttpURLConnection conn = null;

        //
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //Create http connection with the server
        try {
            url = new URL("http://fierce-dawn-82229.herokuapp.com/getnearesttruckslocations");
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject json = new JSONObject();
        OutputStreamWriter out = null;

        //Add data that needs to be sent to the server
        try {
            json.put("loc_latitude", (float) lat);
            json.put("loc_longitude", (float) lng);
            out = new OutputStreamWriter(conn.getOutputStream());
            out.write(json.toString()); //Writes out the string to the underlying output stream as a sequence of bytes
            out.flush(); // Flushes the data output stream.
            out.close(); // Closing the output stream.
        } catch (Exception e) {
            e.printStackTrace();
        }

        StringBuilder sb = new StringBuilder();

        //Get data from server
        try {
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                System.out.println("" + sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONArray json_trucks = null;

        //Parse data into array
        try {
            json_trucks = new JSONArray(sb.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json_trucks;
    }

    public void LaunchLogIn(View view) {
        Intent intent = new Intent(this, LogInActivity.class);
        startActivity(intent);
    }

    public void TruckList(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("lat", lat);
        intent.putExtra("lng", lng);
        startActivity(intent);
    }

    public void LaunchSettings(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void LaunchAddTruck(View view) {
        AccessToken a = AccessToken.getCurrentAccessToken();


        if (a != null) {
            Intent intent = new Intent(this, TruckAddActivity.class);
            startActivity(intent);
        } else {

            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setTitle("Log In");
            builder1.setMessage("You need to be logged in to report a truck");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Log in now",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            Intent intent = new Intent(MainActivity.this, LogInActivity.class);
                            startActivity(intent);
                        }
                    });

            builder1.setNegativeButton(
                    "Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();

        }
    }

    public void Filter(View view) {
        Intent intent = new Intent(this, FilterActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();

        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mGoogleApiClient.isConnected())
            stopLocationUpdates();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mGoogleApiClient.isConnected())
            startLocationUpdates();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");

        if (loc == null)
            loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (loc != null)
            update_lat_long();
        else
            Toast.makeText(this, R.string.no_location_detected, Toast.LENGTH_LONG).show();

        startLocationUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        loc = location;
        update_lat_long();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());

    }
}