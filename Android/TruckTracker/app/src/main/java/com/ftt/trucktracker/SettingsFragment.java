package com.ftt.trucktracker;

import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
Fragment for inserting the correct options for settings
 */
public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }
}
