package com.ftt.trucktracker;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
/*
Menu for displaying trucks in a list view generated based on the current location.
 */
public class MenuActivity extends Activity {
    ListView listView ;
    double lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                lat= -1;
                lng = -1;
            } else {
                lat= extras.getDouble("lat");
                lng= extras.getDouble("lng");
            }
        } else {
            lat= (double) savedInstanceState.getSerializable("lat");
            lng= (double) savedInstanceState.getSerializable("lng");
        }

        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.list);

        // Get nearby truck data from server
        JSONArray jsonArray = getTruckData(lat, lng);
        JSONArray jsonArray_loc = null;
        JSONArray jsonArray_trucks = null;

        String[] trucks = {};
        if (jsonArray != null) {


            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    JSONObject loc = jsonObject.getJSONObject("location");
                    JSONObject truck = jsonObject.getJSONObject("truck");

                    String truck_name = truck.getString("truckname");
                    trucks = Arrays.copyOf(trucks, trucks.length + 1); //create new array from old array and allocate one more element
                    trucks[trucks.length - 1] = truck_name;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, trucks);


        // Assign adapter to ListView
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                int itemPosition     = position;

                // ListView Clicked item value
                String  itemValue    = (String) listView.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :" + itemPosition + "  ListItem : " + itemValue, Toast.LENGTH_LONG)
                        .show();

            }

        });
    }

    public JSONArray getTruckData(double lat, double lng) {
        URL url = null;
        HttpURLConnection conn = null;

        //
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //Create http connection with the server
        try {
            url = new URL("http://fierce-dawn-82229.herokuapp.com/getnearesttruckslocations");
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject json = new JSONObject();
        OutputStreamWriter out = null;

        //Add data that needs to be sent to the server
        try {
            json.put("loc_latitude", (float) lat);
            json.put("loc_longitude", (float) lng);
            out = new OutputStreamWriter(conn.getOutputStream());
            out.write(json.toString()); //Writes out the string to the underlying output stream as a sequence of bytes
            out.flush(); // Flushes the data output stream.
            out.close(); // Closing the output stream.
        } catch (Exception e) {
            e.printStackTrace();
        }

        StringBuilder sb = new StringBuilder();

        //Get data from server
        try {
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                System.out.println("" + sb.toString());
            } else {
                System.out.println(conn.getResponseMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONArray json_trucks = null;

        //Parse data into array
        try {
            json_trucks = new JSONArray(sb.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json_trucks;
    }

}
