package com.ftt.trucktracker;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/*
Activity to add a truck to the back end based on user input.
 */

public class TruckAddActivity extends Activity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    protected static final String TAG = "basic-location-sample";
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    GoogleMap map;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest loc_req;
    protected Location loc;
    protected double lat, lng;
    protected CameraUpdate center;
    protected CameraUpdate zoom;
    MarkerOptions mp;
    private AutoCompleteTextView actv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_sighting);

        actv = (AutoCompleteTextView) findViewById(R.id.list_of_trucks);

        String[] countries = {"Cracked", "Torticas", "Hot Dog Stand", "Tostada"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,countries);
        actv.setAdapter(adapter);

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map_reportSighting)).getMap();
        mp = new MarkerOptions();

        buildGoogleApiClient();

        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {

            @Override
            public void onMapLongClick(LatLng latLng) {
                map.clear();
                mp.position(new LatLng(latLng.latitude, latLng.longitude));
                map.addMarker(mp);
            }
        });

    }


    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    protected void createLocationRequest() {
        loc_req = new LocationRequest();

        loc_req.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        loc_req.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        loc_req.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();

        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mGoogleApiClient.isConnected())
            stopLocationUpdates();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mGoogleApiClient.isConnected())
            startLocationUpdates();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");

        startLocationUpdates();

        if (loc == null)
            loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (loc != null)
            update_lat_long();
        else
            Toast.makeText(this, R.string.no_location_detected, Toast.LENGTH_LONG).show();


    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());

    }

    protected void startLocationUpdates() {

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, loc_req, this);
    }

    protected void stopLocationUpdates() {

        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }


    private void update_lat_long() {
        if (loc != null) {
            lat = loc.getLatitude();
            lng = loc.getLongitude();

        }

        map.clear();

        //Center Camera on user position and set zoom level
        center = CameraUpdateFactory.newLatLng((new LatLng(lat, lng)));
        zoom = CameraUpdateFactory.zoomTo(15);
        map.moveCamera(center);
        map.animateCamera(zoom);

        //Add marker for user location
        mp.position(new LatLng(lat, lng));
        map.addMarker(mp);

    }

    public void Report(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLocationChanged(Location location) {

    }
}
