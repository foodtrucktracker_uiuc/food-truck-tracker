import endpoints
from google.appengine.ext import db
from protorpc import messages
from protorpc import message_types
from protorpc import remote
from models import FoodTruckMessage
from models import FoodTruck

package = 'FoodTruckTracker'


class FoodTruckList(messages.Message):
    items = messages.MessageField(FoodTruckMessage, 1, repeated=True)


@endpoints.api(name='food_truck_tracker', version='v1')
class FoodTruckTrackerAPI(remote.Service):
    """Food Truck Tracker API v1."""

    @endpoints.method(message_types.VoidMessage, FoodTruckList,
                    path='foodTrucks', http_method='GET',
                    name='foodTrucks.getFoodTrucks')
    def food_truck_list(self, unused_request):
        q = db.GqlQuery("SELECT * FROM FoodTruck")

        results = []
        for item in q.run(limit=100):
            results.append(FoodTruckMessage(name=item.name,latitude=item.latitude,longitude=item.longitude))

        return FoodTruckList(items=results)

APPLICATION = endpoints.api_server([FoodTruckTrackerAPI])
