from google.appengine.ext import db
from protorpc import messages
from protorpc import message_types


class FoodTruck(db.Model):
    name = db.StringProperty(indexed=False)
    latitude = db.FloatProperty(indexed=False)
    longitude = db.FloatProperty(indexed=False)


class FoodTruckMessage(messages.Message):
    name = messages.StringField(1)
    latitude = messages.FloatField(2)
    longitude = messages.FloatField(3)


class User(db.Model):
    first_name = db.StringProperty(indexed=False)
    last_name = db.StringProperty(indexed=False)
