#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
from google.appengine.ext import db
import models


class MainHandler(webapp2.RequestHandler):
    def get_index_state_as_string(index_state):
        return {db.Index.BUILDING:'BUILDING', db.Index.SERVING:'SERVING', db.Index.DELETING:'DELETING', db.Index.ERROR:'ERROR'}[index_state]

    def get(self):
        self.response.write('<h1>Food Truck Tracker</h1><br>')
        self.response.write('<h3>Food Trucks</h3>')

        q = db.GqlQuery("SELECT * FROM FoodTruck")
        for item in q.run(limit=100):
            self.response.write(item.name + ' | ' + str(item.latitude) + ' | ' + str(item.longitude) + '<br>')

        self.response.write('<h3>Users</h3>')
        q = db.GqlQuery("SELECT * FROM User")
        for item in q.run(limit=100):
            self.response.write(item.first_name + ' | ' + item.last_name + '<br>')


app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)