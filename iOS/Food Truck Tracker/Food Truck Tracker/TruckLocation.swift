//
//  TruckLocation.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 2/24/16.
//  Copyright © 2016 James Wegner. All rights reserved.
//

import UIKit
import CoreLocation

class TruckLocation: NSObject {
    var locationName: String = ""
    var location: CLLocation = CLLocation()

    var sundayStart: NSDate = NSDate()
    var sundayEnd: NSDate = NSDate()
    var sundayClosed: Bool = false
    
    var mondayStart: NSDate = NSDate()
    var mondayEnd: NSDate = NSDate()
    var mondayClosed: Bool = false

    var tuesdayStart: NSDate = NSDate()
    var tuesdayEnd: NSDate = NSDate()
    var tuesdayClosed: Bool = false

    var wednesdayStart: NSDate = NSDate()
    var wednesdayEnd: NSDate = NSDate()
    var wednesdayClosed: Bool = false

    var thursdayStart: NSDate = NSDate()
    var thursdayEnd: NSDate = NSDate()
    var thursdayClosed: Bool = false

    var fridayStart: NSDate = NSDate()
    var fridayEnd: NSDate = NSDate()
    var fridayClosed: Bool = false

    var saturdayStart: NSDate = NSDate()
    var saturdayEnd: NSDate = NSDate()
    var saturdayClosed: Bool = false
}
