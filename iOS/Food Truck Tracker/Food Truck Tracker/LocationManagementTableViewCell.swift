//
//  LocationManagementTableViewCell.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 2/24/16.
//  Copyright © 2016 James Wegner. All rights reserved.
//

import UIKit

class LocationManagementTableViewCell: UITableViewCell {
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
