//
//  LocationManagerViewController.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 2/24/16.
//  Copyright © 2016 James Wegner. All rights reserved.
//

import UIKit

class LocationManagerViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dateSegmentedControl: UISegmentedControl!
    @IBOutlet weak var closedSwitch: UISwitch!
    
    var truckLocation: TruckLocation = TruckLocation()
    var startDatePicker: UIDatePicker = UIDatePicker()
    var endDatePicker: UIDatePicker = UIDatePicker()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupNavigationView()
    }
    
    func setupNavigationView() {
        self.navigationItem.title = truckLocation.locationName
    }
    
    func setupTableView() {
        self.tableView.tableHeaderView = UIView(frame:CGRectZero)
        self.tableView.tableFooterView = UIView(frame:CGRectZero)
        self.tableView.registerNib(UINib(nibName:"LocationManagementTableViewCell", bundle:NSBundle.mainBundle()), forCellReuseIdentifier:"timeCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let tableViewCell: LocationManagementTableViewCell = tableView.dequeueReusableCellWithIdentifier("timeCell") as! LocationManagementTableViewCell
        
        if(indexPath.section == 0) {
            startDatePicker = tableViewCell.datePicker
            startDatePicker.addTarget(self, action: #selector(LocationManagerViewController.changedStartDate(_:)), forControlEvents: UIControlEvents.ValueChanged)
            
        } else if (indexPath.section == 1) {
            endDatePicker = tableViewCell.datePicker
            endDatePicker.addTarget(self, action: #selector(LocationManagerViewController.changedEndDate(_:)), forControlEvents: UIControlEvents.ValueChanged)
        }
        
        return tableViewCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated:true)
        self.performSegueWithIdentifier("locationManagerSegue", sender:nil)
    }
    

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header: UIView = UIView(frame:CGRectMake(0,0,UIScreen.mainScreen().bounds.width, 45))
        header.backgroundColor = FoodTruckTrackerConstants.foodTruckTrackerRed()
        
        let label: UILabel = UILabel(frame: CGRectMake(0,0,UIScreen.mainScreen().bounds.width, 45))
        label.textColor = UIColor.whiteColor()
        label.textAlignment = .Center
        label.text = ""
        label.font = UIFont(name: "HelveticaNeue-Light", size: 22)

        
        if(section == 0) {
            label.text = "Start Time"
        }else {
            label.text = "End Time"
        }
        
        header.addSubview(label)
        return header
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 214
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    // MARK: IBAction
    
    func changedStartDate(sender: AnyObject) {
        switch dateSegmentedControl.selectedSegmentIndex {
        case 0:
            truckLocation.sundayStart = startDatePicker.date
            break
            
        case 1:
            truckLocation.mondayStart = startDatePicker.date
            break
            
        case 2:
            truckLocation.tuesdayStart = startDatePicker.date
            break
            
        case 3:
            truckLocation.wednesdayStart = startDatePicker.date
            break
            
        case 4:
            truckLocation.thursdayStart = startDatePicker.date
            break
            
        case 5:
            truckLocation.fridayStart = startDatePicker.date
            break
            
        case 6:
            truckLocation.saturdayStart = startDatePicker.date
            break
            
        default:
            break
        }
    }
    
    func changedEndDate(sender: AnyObject) {
        switch dateSegmentedControl.selectedSegmentIndex {
        case 0:
            truckLocation.sundayEnd = endDatePicker.date
            break
            
        case 1:
            truckLocation.mondayEnd = endDatePicker.date
            break
            
        case 2:
            truckLocation.tuesdayEnd = endDatePicker.date
            break
            
        case 3:
            truckLocation.wednesdayEnd = endDatePicker.date
            break
            
        case 4:
            truckLocation.thursdayEnd = endDatePicker.date
            break
            
        case 5:
            truckLocation.fridayEnd = endDatePicker.date
            break
            
        case 6:
            truckLocation.saturdayEnd = endDatePicker.date
            break
            
        default:
            break
        }
    }
    
    @IBAction func didChangeClosedSwitch(sender: AnyObject) {
        if (closedSwitch.on) {
            startDatePicker.alpha = 0
            endDatePicker.alpha = 0
        } else {
            startDatePicker.alpha = 1
            endDatePicker.alpha = 1
        }
    }
    
    @IBAction func didChangeDateControl(sender: AnyObject) {
        switch dateSegmentedControl.selectedSegmentIndex {
        case 0:
            closedSwitch.on = truckLocation.sundayClosed
            startDatePicker.date = truckLocation.sundayStart
            endDatePicker.date = truckLocation.sundayEnd
            break
            
        case 1:
            closedSwitch.on = truckLocation.mondayClosed
            startDatePicker.date = truckLocation.mondayStart
            endDatePicker.date = truckLocation.mondayEnd
            break
            
        case 2:
            closedSwitch.on = truckLocation.tuesdayClosed
            startDatePicker.date = truckLocation.tuesdayStart
            endDatePicker.date = truckLocation.tuesdayEnd
            break
            
        case 3:
            closedSwitch.on = truckLocation.wednesdayClosed
            startDatePicker.date = truckLocation.wednesdayStart
            endDatePicker.date = truckLocation.wednesdayEnd
            break
            
        case 4:
            closedSwitch.on = truckLocation.thursdayClosed
            startDatePicker.date = truckLocation.thursdayStart
            endDatePicker.date = truckLocation.thursdayEnd
            break
            
        case 5:
            closedSwitch.on = truckLocation.fridayClosed
            startDatePicker.date = truckLocation.fridayStart
            endDatePicker.date = truckLocation.fridayEnd
            break
            
        case 6:
            closedSwitch.on = truckLocation.saturdayClosed
            startDatePicker.date = truckLocation.saturdayStart
            endDatePicker.date = truckLocation.saturdayEnd
            break
            
        default:
            break
        }
    }
}
