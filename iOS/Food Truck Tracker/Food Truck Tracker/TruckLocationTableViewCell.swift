//
//  TruckLocationTableViewCell.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 2/24/16.
//  Copyright © 2016 James Wegner. All rights reserved.
//

import UIKit

class TruckLocationTableViewCell: UITableViewCell {
    @IBOutlet weak var locationLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
