//
//  TruckMenuViewController.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 12/8/15.
//  Copyright © 2015 James Wegner. All rights reserved.
//

import UIKit

class TruckMenuViewController: UIViewController {
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var closeBarButton: UIBarButtonItem!
    
    var foodTruck: TruckTrackerFoodTruck = TruckTrackerFoodTruck()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pdfLoc = NSURL(fileURLWithPath:NSBundle.mainBundle().pathForResource("Cracked-Menu-2015", ofType:"pdf")!)
        let request = NSURLRequest(URL: pdfLoc);
        self.webView.loadRequest(request)
        self.closeBarButton.tintColor = UIColor.whiteColor()
        self.setupNavigationBar()
    }

    func setupNavigationBar() {
        self.navigationItem.title = self.foodTruck.name + " Menu"
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = FoodTruckTrackerConstants.foodTruckTrackerRed()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    }
    
    // IBAction
    
    @IBAction func didTapCloseButton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion:nil)
    }
}
