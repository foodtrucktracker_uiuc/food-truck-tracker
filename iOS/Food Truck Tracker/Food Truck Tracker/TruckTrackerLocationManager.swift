//
//  FoodTruckTrackerLocationManager.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 10/27/15.
//  Copyright © 2015 James Wegner. All rights reserved.
//

import UIKit
import CoreLocation

protocol FoodTruckTrackerLocationManagerProtocol {
    func didUpdateUserLocation()
}

class TruckTrackerLocationManager: NSObject, CLLocationManagerDelegate {
    static var locationManagerDelegate = TruckTrackerLocationManager()
    static var locationManager = CLLocationManager()
    static var delegate: FoodTruckTrackerLocationManagerProtocol?
    
    static func startLocationUpdates(){
        TruckTrackerLocationManager.locationManager.delegate = TruckTrackerLocationManager.locationManagerDelegate
        TruckTrackerLocationManager.locationManager.desiredAccuracy = 5000
        TruckTrackerLocationManager.locationManager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        switch status {
        case CLAuthorizationStatus.AuthorizedAlways:
                print("Location authorized always")
                TruckTrackerLocationManager.startLocationUpdates()
            
        case CLAuthorizationStatus.AuthorizedWhenInUse:
                print("Location authorized when in use")
                TruckTrackerLocationManager.startLocationUpdates()
            
        case CLAuthorizationStatus.Denied:
                print("Location authorization denied")
        
        default:
            print("Location authorization unknown")
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(locations.last != nil){
            FoodTruckTrackerDatastore.userLocation = locations.last!
            TruckTrackerLocationManager.delegate?.didUpdateUserLocation()
        }
    }
}
