//
//  TruckTrackerServiceCallParser.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 3/27/16.
//  Copyright © 2016 James Wegner. All rights reserved.
//

import UIKit
import CoreLocation

class TruckTrackerServiceCallParser: NSObject {
    
    static func parseGetFoodTrucks(response: NSArray) -> NSMutableArray? {
        if (response.count == 0) {
            return nil
        }
        
        let foodTrucks: NSMutableArray = NSMutableArray()
        
        for(var i = 0; i < response.count; i++) {
            do {
                let responseDictionary: NSDictionary = response.objectAtIndex(i) as! NSDictionary
                
                let locationDictionary: NSDictionary = responseDictionary.objectForKey("location") as! NSDictionary
                
        
                let sightingDictionary: NSDictionary = responseDictionary.objectForKey("sighting") as! NSDictionary
                let truckDictionary: NSDictionary = responseDictionary.objectForKey("truck") as! NSDictionary
                
                let foodTruck: TruckTrackerFoodTruck = TruckTrackerFoodTruck()
                
                foodTruck.name = truckDictionary["truckname"] as! String
                foodTruck.location = CLLocation(latitude:locationDictionary["loc_latitude"] as! Double, longitude: locationDictionary["loc_longitude"] as! Double)
                foodTruck.userReported = !(truckDictionary["is_admin_controlled"] as! Bool)
                
                let formatter = NSDateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                var sightString: String = sightingDictionary.objectForKey("sight_time") as! String
                let endIndex = sightString.endIndex.advancedBy(-5)
                sightString = sightString.substringToIndex(endIndex)
                let sightDate = formatter.dateFromString(sightString)
                
                if (sightDate != nil) {
                    foodTruck.sightTime = sightDate!
                } else {
                    print("Nil sight_time")
                }
                
                foodTrucks.addObject(foodTruck)
                
            } catch {
                
            }
        }
        
        return foodTrucks
    }
    
    static func parseGetCompanyFoodTrucks(response: NSArray) -> NSMutableArray? {
        if (response.count == 0) {
            return nil
        }
        print(response)
        print(response.count)
        let foodTruckLocations: NSMutableArray = NSMutableArray()
        
        for(var i = 0; i < response.count; i++) {
            let responseDictionary: NSDictionary = response.objectAtIndex(i) as! NSDictionary
            let foodTruckLocation: TruckLocation = TruckLocation()
            foodTruckLocation.locationName = responseDictionary.objectForKey("truckname") as! String
            foodTruckLocations.addObject(foodTruckLocation)
        }
        
        return foodTruckLocations
    }
}
