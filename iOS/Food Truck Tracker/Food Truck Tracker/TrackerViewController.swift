//
//  ViewController.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 10/26/15.
//  Copyright © 2015 James Wegner. All rights reserved.
//

import UIKit
import GoogleMaps;

class TruckTrackerViewController: UIViewController, FoodTruckTrackerLocationManagerProtocol, GMSMapViewDelegate {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var reportTruckButton: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var updatedUserLocation = false
    var foodTrucks:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TruckTrackerLocationManager.delegate = self
        TruckTrackerLocationManager.startLocationUpdates()
        setupMapView()
        setupNavigationBar()
        setupFilterButton()
        setupListButton()
        setupReportTruckButton()
        self.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
    }
    
    override func viewDidAppear(animated: Bool) {
        updateTrucks()
    }
    
    // view setup
    
    func setupMapView() {
        mapView.myLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.buildingsEnabled = false
        mapView.delegate = self
    }
    
    func setupLoadingIndicator() {
        loadingIndicator.color = FoodTruckTrackerConstants.foodTruckTrackerRed()
        loadingIndicator.startAnimating()
        loadingIndicator.alpha = 0
    }

    func setupNavigationBar() {
        self.navigationItem.title = "Food Truck Tracker"
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = FoodTruckTrackerConstants.foodTruckTrackerRed()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        let loginButton = UIBarButtonItem()
        loginButton.title = "Login"
        loginButton.target = self
        loginButton.action = #selector(TruckTrackerViewController.didTapLoginButton)
        loginButton.tintColor = UIColor.whiteColor()
        
        let settingsButton = UIBarButtonItem()
        settingsButton.title = "Settings"
        settingsButton.target = self
        settingsButton.action = #selector(TruckTrackerViewController.didTapSettingsButton)
        settingsButton.tintColor = UIColor.whiteColor()
        
        self.navigationItem.leftBarButtonItem = loginButton
        self.navigationItem.rightBarButtonItem = settingsButton
    }
    
    func setupFilterButton() {
        self.filterButton.titleLabel?.text = "Filter"
        self.filterButton.titleLabel?.font = UIFont.systemFontOfSize(18)
        self.filterButton.tintColor = UIColor.whiteColor()
        self.filterButton.backgroundColor = UIColor.darkGrayColor()
        self.filterButton.layer.cornerRadius = 5
        self.filterButton.layer.borderWidth = 2
        self.filterButton.layer.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.8).CGColor
        self.filterButton.addTarget(self, action:#selector(TruckTrackerViewController.didTapFilterButton), forControlEvents:UIControlEvents.TouchUpInside)
    }
    
    func setupListButton() {
        self.listButton.titleLabel?.text = "List"
        self.listButton.titleLabel?.font = UIFont.systemFontOfSize(18)
        self.listButton.tintColor = UIColor.whiteColor()
        self.listButton.backgroundColor = UIColor.darkGrayColor()
        self.listButton.layer.cornerRadius = 5
        self.listButton.layer.borderWidth = 2
        self.listButton.layer.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.8).CGColor
    }
    
    func setupReportTruckButton() {
        self.reportTruckButton.titleLabel?.text = "Add Truck"
        self.reportTruckButton.titleLabel?.font = UIFont.systemFontOfSize(18)
        self.reportTruckButton.tintColor = UIColor.whiteColor()
        self.reportTruckButton.backgroundColor = UIColor.darkGrayColor().colorWithAlphaComponent(0.9)
        self.reportTruckButton.layer.cornerRadius = 5
        self.reportTruckButton.layer.borderWidth = 2
        self.reportTruckButton.layer.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.8).CGColor
    }
    
    func updateTruckPins() {
        for (index, foodTruck) in foodTrucks.enumerate() {
            let foodTruck: TruckTrackerFoodTruck = foodTruck as! TruckTrackerFoodTruck

            let marker:GMSMarker = GMSMarker(position: foodTruck.location.coordinate)
            marker.title = foodTruck.name
            marker.map = mapView;
            marker.userData = index
            
            if(foodTruck.userReported) {
                marker.icon = GMSMarker.markerImageWithColor(UIColor.blueColor())
                
                let formatter = NSDateFormatter()
                formatter.dateFormat = "hh:mm aa"
                let sightTimeString = formatter.stringFromDate(foodTruck.sightTime)
                marker.snippet = "User added at " + sightTimeString

            } else {
                marker.snippet = "Open until 4:00pm"
                marker.icon = GMSMarker.markerImageWithColor(UIColor.redColor())
            }
        }
    }
    
    // Data
    
    func updateTrucks() {
        if (updatedUserLocation) {
            loadingIndicator.alpha = 1
            
            let foodTrucks = NSMutableArray()
            
            let truck1: TruckTrackerFoodTruck = TruckTrackerFoodTruck()
            truck1.location = CLLocation(latitude:40.110131, longitude:-88.225550)
            truck1.name = "Cracked"
            truck1.street = "Mathews & Springfield"
            truck1.phoneNumber = "8475423161"
            truck1.menu = "Cracked-Menu-2015"
            
            let truck2: TruckTrackerFoodTruck = TruckTrackerFoodTruck()
            truck2.location = CLLocation(latitude:40.110303, longitude:-88.227266)
            truck2.name = "Burrito King"
            truck2.street = "Fake St."
            truck2 .phoneNumber = "8475423161"
            truck2.menu = "Cracked-Menu-2015"
            
            let truck3: TruckTrackerFoodTruck = TruckTrackerFoodTruck()
            truck3.location = CLLocation(latitude:40.109557, longitude:-88.223844)
            truck3.name = "Korean BBQ Tacos"
            truck3.street = "Fake St."
            truck3.phoneNumber = "8475423161"
            truck3.menu = "Cracked-Menu-2015"
            truck3.userReported = true
            
            let truck4: TruckTrackerFoodTruck = TruckTrackerFoodTruck()
            truck4.location = CLLocation(latitude:40.110394, longitude:-88.223104)
            truck4.name = "Caribbean Grill"
            truck4.street = "Fake St."
            truck4.phoneNumber = "8475423161"
            truck4.menu = "Cracked-Menu-2015"
            
            let truck5: TruckTrackerFoodTruck = TruckTrackerFoodTruck()
            truck5.location = CLLocation(latitude:40.109926, longitude:-88.228886)
            truck5.name = "Cracked"
            truck5.street = "Fake St."
            truck5.phoneNumber = "8475423161"
            truck5.menu = "Cracked-Menu-2015"
            truck5.userReported = true
            
            foodTrucks.addObject(truck1)
            foodTrucks.addObject(truck2)
            foodTrucks.addObject(truck3)
            foodTrucks.addObject(truck4)
            foodTrucks.addObject(truck5)
            
            self.foodTrucks = foodTrucks
            self.updateTruckPins()
            self.loadingIndicator.alpha = 0

            /*TruckTrackerServiceCall.getFoodTrucks({(results: NSMutableArray?) in
                self.loadingIndicator.alpha = 0
                if (results != nil) {
                    print("Trucks updated")
                    self.foodTrucks = results!
                    self.updateTruckPins()
                } else {
                    print("No trucks returned")
                }
            })*/
        }
    }
    
    // FoodTruckTrackerLocationManager Delegate

    func didUpdateUserLocation() {
        let coordinate = FoodTruckTrackerDatastore.userLocation.coordinate
        if (!updatedUserLocation){
            updatedUserLocation = true
            let camera = GMSCameraPosition.cameraWithLatitude(coordinate.latitude, longitude: coordinate.longitude, zoom: 15)
            mapView.camera = camera
            
            updateTrucks()
        }
    }
    
    // MapView Delegate
    
    func mapView(mapView: GMSMapView!, didTapInfoWindowOfMarker marker: GMSMarker!) {
        let index: Int = marker.userData as! Int
        self.performSegueWithIdentifier("truckView", sender:foodTrucks.objectAtIndex(index))
    }
    
    // IBAction
    
    func didTapLoginButton() {
        self.performSegueWithIdentifier("loginView", sender:nil)
    }
    
    func didTapSettingsButton() {
        self.performSegueWithIdentifier("settings", sender:nil)
    }
    
    func didTapFilterButton() {
        let optionMenu = UIAlertController(title:"Food truck filters", message: nil, preferredStyle: .ActionSheet)
        
        let allOptions = UIAlertAction(title: "All", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        let vegetarian = UIAlertAction(title: "Vegetarian", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        let vegan = UIAlertAction(title: "Vegan", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        let glutenFree = UIAlertAction(title: "Gluten Free", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
        })

        optionMenu.addAction(allOptions)
        optionMenu.addAction(vegetarian)
        optionMenu.addAction(vegan)
        optionMenu.addAction(glutenFree)
        
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func didTapReportTruckButton(sender: AnyObject) {
        if (FoodTruckTrackerDatastore.userIsLoggedIn()) {
            self.performSegueWithIdentifier("reportSegue", sender:nil)

        } else {
            let alertController = UIAlertController(title: "You must login to Facebook before adding a food truck", message: "", preferredStyle: .Alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action) in
            }
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    // Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "truckView" {
            let truckViewController: TruckViewController = segue.destinationViewController as! TruckViewController
            truckViewController.foodTruck = sender as! TruckTrackerFoodTruck
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            
        }else if(segue.identifier == "foodTruckList") {
            let navController = segue.destinationViewController as! UINavigationController
            let truckListController: FoodTruckTrackerListViewController = navController.topViewController as! FoodTruckTrackerListViewController
            truckListController.foodTrucks = foodTrucks
        
        }else if(segue.identifier == "reportSegue") {
            let navController = segue.destinationViewController as! UINavigationController
        }
    }
}

