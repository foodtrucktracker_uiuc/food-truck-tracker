//
//  FoodTruckTrackerOwnerLoginViewController.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 2/2/16.
//  Copyright © 2016 James Wegner. All rights reserved.
//

import UIKit

class FoodTruckTrackerOwnerLoginViewController: UIViewController {
    @IBOutlet weak var loginButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
    }

    func setupNavigationBar() {
        self.navigationItem.title = "Owner Login"
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = FoodTruckTrackerConstants.foodTruckTrackerRed()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    }
    
    @IBAction func didTapLoginButton(sender: AnyObject) {
    }
}
