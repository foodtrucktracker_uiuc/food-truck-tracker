//
//  FoodTruckTrackerConstants.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 10/27/15.
//  Copyright © 2015 James Wegner. All rights reserved.
//

import UIKit

class FoodTruckTrackerConstants: NSObject {
    static func foodTruckTrackerRed() -> UIColor {
        return UIColor(red:247.0/255.0, green:58.0/255.0, blue:88.0/255.0, alpha:1.0)
    }
}
