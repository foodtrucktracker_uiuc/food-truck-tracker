//
//  FoodTruckTrackerLoginController.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 11/4/15.
//  Copyright © 2015 James Wegner. All rights reserved.
//

import UIKit

class FoodTruckTrackerLoginController: UIViewController, FBSDKLoginButtonDelegate{
    @IBOutlet weak var fbLoginButton: FBSDKLoginButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupFBLoginButton()
    }
    
    func setupNavigationBar() {
        self.navigationItem.title = "Login"
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = FoodTruckTrackerConstants.foodTruckTrackerRed()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
        let cancelButton = UIBarButtonItem()
        cancelButton.title = "Cancel"
        cancelButton.target = self
        cancelButton.action = #selector(FoodTruckTrackerLoginController.didTapCancelButton)
        cancelButton.tintColor = UIColor.whiteColor()
        
        self.navigationItem.leftBarButtonItem = cancelButton
        
        let truckOwnerButton = UIBarButtonItem()
        truckOwnerButton.title = "Truck Owners"
        truckOwnerButton.target = self
        truckOwnerButton.action = #selector(FoodTruckTrackerLoginController.didTapOwnerButton)
        truckOwnerButton.tintColor = UIColor.whiteColor()
        
        self.navigationItem.rightBarButtonItem = truckOwnerButton
    }
    
    func setupFBLoginButton() {
        fbLoginButton.readPermissions = ["public_profile", "email"]
        fbLoginButton.delegate = self
        fbLoginButton.loginBehavior = FBSDKLoginBehavior.Native
    }
    
    func didTapCancelButton(){
        self.dismissViewControllerAnimated(true, completion:nil)
    }
    
    func didTapOwnerButton() {
        if (FoodTruckTrackerDatastore.userIsLoggedIn()) {
            
            if (FoodTruckTrackerDatastore.companySK() == nil || FoodTruckTrackerDatastore.companySK() == "") {
                let alertController = UIAlertController(title: "No company found", message: "Tap Ok to create your company", preferredStyle: .Alert)
                let cancelAction = UIAlertAction(title: "Cancel", style: .Default) { (action) in
                }
                let createAction = UIAlertAction(title: "Ok", style: .Cancel) { (action) in
                    self.performSegueWithIdentifier("companyCreationViewSegue", sender:nil)
                }
                alertController.addAction(createAction)
                alertController.addAction(cancelAction)
                self.presentViewController(alertController, animated: true, completion: nil)
                
            } else {
                self.performSegueWithIdentifier("companyOwnerViewSegue", sender:nil)
            }

            
        } else {
            let alertController = UIAlertController(title: "Please login to Facebook first", message: "", preferredStyle: .Alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action) in
            }
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: FBSDKLoginButtonDelegate
    
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if (error != nil) {
            print("Error logging in: %@", error.description)
        } else {
            print(String(format: "User logged in with permissions: %@ \ntoken: %@", result.grantedPermissions, result.token.tokenString));
            FoodTruckTrackerDatastore.setFacebookToken(result.token.tokenString)
            FoodTruckTrackerDatastore.setUserLoggedIn(true)

            let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email"], tokenString: result.token.tokenString, version: nil, HTTPMethod: "GET")
            req.startWithCompletionHandler({ (connection, fbResult, error : NSError!) -> Void in
                if(error == nil) {
                    let email = fbResult.valueForKey("email") as! String
                    FoodTruckTrackerDatastore.setEmail(email)
                    TruckTrackerServiceCall.createUser(email, fbToken: result.token.tokenString, completion:{() in
                    })
                    
                } else {
                    print("FB graph error: ", error.description)
                }
            })
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User logged out")
        FoodTruckTrackerDatastore.setUserLoggedIn(false)
    }
}
