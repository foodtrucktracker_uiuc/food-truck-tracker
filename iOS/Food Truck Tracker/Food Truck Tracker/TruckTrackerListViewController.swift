//
//  FoodTruckTrackerListViewController.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 11/17/15.
//  Copyright © 2015 James Wegner. All rights reserved.
//

import UIKit

class FoodTruckTrackerListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!

    var foodTrucks:NSMutableArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupTableView()
    }
    
    func setupNavigationBar() {
        self.navigationItem.title = "Food Trucks"
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = FoodTruckTrackerConstants.foodTruckTrackerRed()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        let closeButton = UIBarButtonItem()
        closeButton.title = "Close"
        closeButton.target = self
        closeButton.action = #selector(FoodTruckTrackerListViewController.didTapCloseButton)
        closeButton.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem = closeButton
    }
    
    func setupTableView() {
        self.tableView.tableHeaderView = UIView(frame:CGRectZero)
        self.tableView.tableFooterView = UIView(frame:CGRectZero)
        self.tableView.registerNib(UINib(nibName:"FoodTruckTableViewCell", bundle:NSBundle.mainBundle()), forCellReuseIdentifier:"truckCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    // IBAction
    
    func didTapCloseButton() {
        dismissViewControllerAnimated(true, completion:nil)
    }
    
    // TableView Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated:true)
        self.performSegueWithIdentifier("truckView", sender:foodTrucks.objectAtIndex(indexPath.row))
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 56
    }
    
    // TableView Datasource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodTrucks.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let truckCell:FoodTruckTableViewCell = tableView.dequeueReusableCellWithIdentifier("truckCell") as! FoodTruckTableViewCell
        
        let foodTruck: TruckTrackerFoodTruck = foodTrucks.objectAtIndex(indexPath.row) as! TruckTrackerFoodTruck
        truckCell.truckNameLabel.text = foodTruck.name
        
        return truckCell
    }
    
    // Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "truckView" {
            let truckViewController: TruckViewController = segue.destinationViewController as! TruckViewController;
            truckViewController.foodTruck = sender as! TruckTrackerFoodTruck
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
        }
    }
}
