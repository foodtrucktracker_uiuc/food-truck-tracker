//
//  FoodTruckTrackerFoodTruck.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 11/11/15.
//  Copyright © 2015 James Wegner. All rights reserved.
//

import UIKit
import CoreLocation

class TruckTrackerFoodTruck: NSObject {
    var name: String = ""
    var location: CLLocation = CLLocation()
    var street: String = ""
    var phoneNumber: String = ""
    var menu: String = ""
    var userReported: Bool = false
    var sightTime: NSDate = NSDate()
    var openUntil: NSDate = NSDate()
}
