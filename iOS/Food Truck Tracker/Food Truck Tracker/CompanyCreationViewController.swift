//
//  CompanyCreationViewController.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 4/12/16.
//  Copyright © 2016 James Wegner. All rights reserved.
//

import UIKit

class CompanyCreationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var nameField: UITextField = UITextField()
    var phoneField: UITextField = UITextField()
    var zipcodeField: UITextField = UITextField()
    var cityField: UITextField = UITextField()
    var stateField: UITextField = UITextField()
    var emailField: UITextField = UITextField()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationView()
        setupTableView()
    }
    
    func setupNavigationView() {
        self.navigationItem.title = "Add Company"
        
        
        let addButton = UIBarButtonItem()
        addButton.title = "Add"
        addButton.target = self
        addButton.action = #selector(CompanyCreationViewController.didTapAddButton)
        addButton.tintColor = UIColor.whiteColor()
        self.navigationItem.rightBarButtonItem = addButton
    }

    func setupTableView() {
        self.tableView.tableHeaderView = UIView(frame:CGRectZero)
        self.tableView.tableFooterView = UIView(frame:CGRectZero)
        self.tableView.registerNib(UINib(nibName:"CompanyFieldTableViewCell", bundle:NSBundle.mainBundle()), forCellReuseIdentifier:"fieldCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let fieldCell:CompanyFieldTableViewCell = tableView.dequeueReusableCellWithIdentifier("fieldCell") as! CompanyFieldTableViewCell
        
        switch indexPath.row {
        case 0:
            fieldCell.infoTextField.placeholder = "Company name"
            fieldCell.infoTextField.keyboardType = UIKeyboardType.Default
            nameField = fieldCell.infoTextField
        case 1:
            fieldCell.infoTextField.placeholder = "Phone"
            fieldCell.infoTextField.keyboardType = UIKeyboardType.PhonePad
            phoneField = fieldCell.infoTextField
        /*case 2:
            fieldCell.infoTextField.placeholder = "Email"
            fieldCell.infoTextField.keyboardType = UIKeyboardType.EmailAddress
            emailField = fieldCell.infoTextField*/
        case 2:
            fieldCell.infoTextField.placeholder = "City"
            fieldCell.infoTextField.keyboardType = UIKeyboardType.Default
            cityField = fieldCell.infoTextField
        case 3:
            fieldCell.infoTextField.placeholder = "Zipcode"
            fieldCell.infoTextField.keyboardType = UIKeyboardType.NumberPad
            zipcodeField = fieldCell.infoTextField
        case 4:
            fieldCell.infoTextField.placeholder = "State"
            fieldCell.infoTextField.keyboardType = UIKeyboardType.Default
            stateField = fieldCell.infoTextField
        default:
            break
        }
        
        //let foodTruck: TruckTrackerFoodTruck = foodTrucks.objectAtIndex(indexPath.row) as! TruckTrackerFoodTruck
        //truckCell.truckNameLabel.text = foodTruck.name
        
        return fieldCell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 56
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    // MARK: - Navigation
    
    func didTapAddButton() {
        if (nameField.text?.characters.count > 0 && phoneField.text?.characters.count > 0 && /*emailField.text?.characters.count > 0 &&*/ cityField.text?.characters.count > 0 && zipcodeField.text?.characters.count > 0) {
            
            TruckTrackerServiceCall.createCompany(nameField.text!, phone: phoneField.text!, email: "", city: cityField.text!, zipcode: zipcodeField.text!, state: stateField.text!, completion: {(companySK: String) in
                    FoodTruckTrackerDatastore.setCompanySK(companySK)
                    self.dismissViewControllerAnimated(true, completion: nil)
                })
            
        } else {
            let alertController = UIAlertController(title: "Please fill in all fields first", message: "", preferredStyle: .Alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action) in
            }
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 

}
