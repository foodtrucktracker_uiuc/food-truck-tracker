//
//  TruckTrackerCompany.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 4/13/16.
//  Copyright © 2016 James Wegner. All rights reserved.
//

import UIKit

class TruckTrackerCompany: NSObject {
    var name: String = ""
    var phoneNumber: String = ""
    var email: String = ""
    var foodTrucks: NSMutableArray = NSMutableArray()
}
