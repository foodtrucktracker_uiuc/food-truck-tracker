//
//  FoodTruckTrackerReportViewController.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 1/29/16.
//  Copyright © 2016 James Wegner. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class FoodTruckTrackerReportViewController: UIViewController, GMSMapViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var closeBarButton: UIBarButtonItem!
    @IBOutlet weak var foodTruckNameField: UITextField!
    @IBOutlet weak var addTruckLocationBarButton: UIBarButtonItem!
    
    var truckLocation = CLLocation()
    var truckMarker = GMSMarker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextField()
        setupNavigationBar()
        setupMapView()
        mapView.delegate = self
    }
    
    func setupTextField() {
        foodTruckNameField.layer.borderColor = UIColor.whiteColor().CGColor
        foodTruckNameField.tintColor = FoodTruckTrackerConstants.foodTruckTrackerRed()
        foodTruckNameField.addTarget(self, action: "textFieldDidChange:", forControlEvents: UIControlEvents.EditingChanged)
        addTruckLocationBarButton.enabled = false
    }
    
    func setupNavigationBar() {
        self.navigationItem.title = ""
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = FoodTruckTrackerConstants.foodTruckTrackerRed()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
    }
    
    func setupMapView() {
        mapView.myLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.buildingsEnabled = false
        mapView.delegate = self
        
        let coordinate = FoodTruckTrackerDatastore.userLocation.coordinate
        let camera = GMSCameraPosition.cameraWithLatitude(coordinate.latitude, longitude: coordinate.longitude, zoom: 18)
        mapView.camera = camera
        
        let centerCoordniate = mapView.projection.coordinateForPoint(mapView.center)

        truckMarker = GMSMarker(position:CLLocationCoordinate2D(latitude:centerCoordniate.latitude, longitude:centerCoordniate.longitude))
        truckMarker.title = "Truck Location";
        truckMarker.icon = GMSMarker.markerImageWithColor(UIColor.blueColor())
        truckMarker.map = mapView;
    }
    
    // MARK: GMSMapViewDelegate
    
    func mapView(mapView: GMSMapView, didChangeCameraPosition position: GMSCameraPosition) {
        let centerCoordniate = mapView.projection.coordinateForPoint(mapView.center)
        truckMarker.position = CLLocationCoordinate2D(latitude:centerCoordniate.latitude, longitude:centerCoordniate.longitude)
        truckLocation = CLLocation(latitude:truckMarker.position.latitude, longitude:truckMarker.position.longitude)
    }
    
    // MARK: UITextFIeld Delegate
    
    func textFieldDidChange(textField: UITextField) {
        if(foodTruckNameField.text!.characters.count > 0) {
            addTruckLocationBarButton.enabled = true
        } else {
            addTruckLocationBarButton.enabled = false
        }
    }
    
    // MARK: IBActions
    
    @IBAction func didTapCloseBarButton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion:nil)
    }
    
    @IBAction func didTapAddTruckLocationButton(sender: AnyObject) {
        TruckTrackerServiceCall.addFoodTruckSighting(foodTruckNameField.text!, location:truckLocation.coordinate, completion:{() in
            self.dismissViewControllerAnimated(true, completion:nil)
        })
    }
}
