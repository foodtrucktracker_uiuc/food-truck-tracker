//
//  TruckViewController.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 11/11/15.
//  Copyright © 2015 James Wegner. All rights reserved.
//

import UIKit

class TruckViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var foodTruck: TruckTrackerFoodTruck = TruckTrackerFoodTruck()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = self.foodTruck.name
        //self.navigationItem.prompt = self.foodTruck.street
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.setupTableView()
        self.setupNavigationBar()
    }
    
    func setupTableView() {
        self.tableView.tableHeaderView = UIView(frame:CGRectZero)
        self.tableView.tableFooterView = UIView(frame:CGRectZero)
        self.tableView.registerNib(UINib(nibName:"HoursTodayTableViewCell", bundle:NSBundle.mainBundle()), forCellReuseIdentifier:"hoursTodayCell")
        self.tableView.registerNib(UINib(nibName:"InfoTableViewCell", bundle:NSBundle.mainBundle()), forCellReuseIdentifier:"infoCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func setupNavigationBar() {
        let followButton = UIBarButtonItem()
        followButton.title = "Follow Truck"
        followButton.target = self
        followButton.action = #selector(TruckViewController.didTapFollowTruckButton)
        followButton.tintColor = UIColor.whiteColor()
        self.navigationItem.rightBarButtonItem = followButton
    }
    
    func didTapFollowTruckButton() {
        
    }
    
    // TableView Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated:true)
        switch indexPath.row {
        case 0:
            break
        case 1:
            let phone = "telprompt://" + foodTruck.phoneNumber;
            let url:NSURL = NSURL(string:phone)!;
            UIApplication.sharedApplication().openURL(url);
            break
        case 2:
            break
        case 3:
            self.performSegueWithIdentifier("menuSegue", sender: foodTruck)
            break
        default:
            break
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 124
        case 1:
            return 56
        case 2:
            return 56
        case 3:
            return 56
        default:
            return 56
        }
    }
    
    // TableView Datasource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let hoursTodayCell:HoursTodayTableViewCell = tableView.dequeueReusableCellWithIdentifier("hoursTodayCell") as! HoursTodayTableViewCell
            hoursTodayCell.titleLabel.text = "Hours Today"
            hoursTodayCell.hoursLabel.text = "8:00 am - 4:00 pm"
            hoursTodayCell.selectionStyle = UITableViewCellSelectionStyle.None
            return hoursTodayCell
        case 1:
            let phoneCell: InfoTableViewCell = tableView.dequeueReusableCellWithIdentifier("infoCell") as! InfoTableViewCell
            phoneCell.infoLabel.text = "Call: 847-542-3161"
            phoneCell.iconImageView?.image = UIImage(named:"telephone.png")
            return phoneCell
        case 2:
            let hoursCell: InfoTableViewCell = tableView.dequeueReusableCellWithIdentifier("infoCell") as! InfoTableViewCell
            hoursCell.infoLabel.text = "Hours"
            hoursCell.iconImageView?.image = UIImage(named:"clock.png")
            return hoursCell
        case 3:
            let menuCell: InfoTableViewCell = tableView.dequeueReusableCellWithIdentifier("infoCell") as! InfoTableViewCell
            menuCell.infoLabel.text = "Menu"
            menuCell.iconImageView?.image = UIImage(named:"menu.png")
            return menuCell
        default:
            return UITableViewCell()
        }
    }
    
    // Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.identifier {
        case "menuSegue"?:
            let navController = segue.destinationViewController as! UINavigationController
            let menuController = navController.topViewController as! TruckMenuViewController
            menuController.foodTruck = self.foodTruck
            break
        default:
            break
        }
    }
}
