//
//  OwnerViewController.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 2/19/16.
//  Copyright © 2016 James Wegner. All rights reserved.
//

import UIKit

class OwnerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var company = TruckTrackerCompany()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupTableView()
        getLocations()
        
        // TEST:
        company.name = "Cracked" 
    }
    
    override func viewDidAppear(animated: Bool) {
        self.tableView.reloadData()
    }
    
    func setupNavigationBar() {
        self.navigationItem.title = "Truck Locations"
        let addButton = UIBarButtonItem()
        addButton.title = "Add Location"
        addButton.target = self
        addButton.action = #selector(OwnerViewController.didTapAddButton)
        addButton.tintColor = UIColor.whiteColor()
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    func setupTableView() {
        self.tableView.tableHeaderView = UIView(frame:CGRectZero)
        self.tableView.tableFooterView = UIView(frame:CGRectZero)
        self.tableView.registerNib(UINib(nibName:"TruckLocationTableViewCell", bundle:NSBundle.mainBundle()), forCellReuseIdentifier:"locationCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    // TODO: Implement with backend
    func getLocations() {
        TruckTrackerServiceCall.getCompanyFoodTrucks(FoodTruckTrackerDatastore.companySK()!, completion: {(results: NSMutableArray?) in
            if (results != nil) {
                self.company.foodTrucks = results!
                self.tableView.reloadData()
            }
        })
    }
    
    // MARK: IBAction
    
    func didTapAddButton() {
        self.performSegueWithIdentifier("addLocationSegue", sender: self.company)
    }
    
    // MARK: UITableViewDelegate
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let tableViewCell: TruckLocationTableViewCell = tableView.dequeueReusableCellWithIdentifier("locationCell") as! TruckLocationTableViewCell
        let location: TruckLocation = self.self.company.foodTrucks.objectAtIndex(indexPath.row) as! TruckLocation
        
        tableViewCell.locationLabel.text = location.locationName
        return tableViewCell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated:true)
        self.performSegueWithIdentifier("locationManagerSegue", sender:self.company.foodTrucks.objectAtIndex(indexPath.row))
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.company.foodTrucks.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 64
    }
    
    // MARK: Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "locationManagerSegue") {
            let locationManager: LocationManagerViewController = segue.destinationViewController as! LocationManagerViewController
            locationManager.truckLocation = sender as! TruckLocation
            
        } else if (segue.identifier == "addLocationSegue") {
            let navController: UINavigationController = segue.destinationViewController as! UINavigationController
            let addLocation: TruckManagerAddTruckViewController = navController.topViewController as! TruckManagerAddTruckViewController
            addLocation.company = self.company
        }
    }
}
