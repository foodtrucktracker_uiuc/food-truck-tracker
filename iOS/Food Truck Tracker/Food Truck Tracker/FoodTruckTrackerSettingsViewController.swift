//
//  FoodTruckTrackerSettingsViewController.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 11/26/15.
//  Copyright © 2015 James Wegner. All rights reserved.
//

import UIKit

class FoodTruckTrackerSettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var followedTrucks:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupTableView()
    }
    
    func setupNavigationBar() {
        self.navigationItem.title = "Settings"
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = FoodTruckTrackerConstants.foodTruckTrackerRed()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        let cancelButton = UIBarButtonItem()
        cancelButton.title = "Cancel"
        cancelButton.target = self
        cancelButton.action = Selector("didTapCancelButton")
        cancelButton.tintColor = UIColor.whiteColor()
        
        self.navigationItem.leftBarButtonItem = cancelButton
    }
    
    func didTapCancelButton() {
        self .dismissViewControllerAnimated(true, completion:nil)
    }
    
    func setupTableView() {
        self.tableView.tableHeaderView = UIView(frame:CGRectZero)
        self.tableView.tableFooterView = UIView(frame:CGRectZero)
        self.tableView.registerNib(UINib(nibName:"NotificationDistanceTableViewCell", bundle:NSBundle.mainBundle()), forCellReuseIdentifier:"notificationDistanceCell")
        self.tableView.registerNib(UINib(nibName:"FollowedTruckTableViewCell", bundle:NSBundle.mainBundle()), forCellReuseIdentifier:"followedTruckCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.backgroundColor = UIColor(red:240/255.0, green:240/255.0, blue:240/255.0, alpha: 1)
    }
    
    // TableView Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated:true)
        //self.performSegueWithIdentifier("truckView", sender:foodTrucks.objectAtIndex(indexPath.row))
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 77
        case 1:
            return 44
        default:
            return 0
        }
    }
    
    // TableView Datasource
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height: tableView.frame.size.height)) as UIView
        footer.backgroundColor = UIColor.clearColor()
        return footer
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 44
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
            case 0:
                return 1
            case 1:
                return 5//followedTrucks.count
            default:
                return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
            case 0:
                let distanceCell:NotificationDistanceTableViewCell = tableView.dequeueReusableCellWithIdentifier("notificationDistanceCell") as! NotificationDistanceTableViewCell
                distanceCell.selectionStyle = UITableViewCellSelectionStyle.None
                return distanceCell
            case 1:
                let followedTruckCell:FollowedTruckTableViewCell = tableView.dequeueReusableCellWithIdentifier("followedTruckCell") as! FollowedTruckTableViewCell
                followedTruckCell.selectionStyle = UITableViewCellSelectionStyle.None
                followedTruckCell.truckNameLabel.text = "Truck " + String(indexPath.row+1)
                return followedTruckCell
            default:
                return UITableViewCell()
        }
    }
    
    // Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}
