//
//  FoodTruckTrackerServiceCall.swift
//  
//
//  Created by James Wegner on 11/11/15.
//
//

import UIKit
import CoreLocation


class TruckTrackerServiceCall: NSObject {
    
    /**
     Get the closest food trucks to the user's current location
     **/
    static func getFoodTrucks(completion: (result: NSMutableArray?) -> Void) {
        print("Getting food trucks")
        let urlPath: String = "http://fierce-dawn-82229.herokuapp.com/getnearesttruckslocations"
        let url: NSURL = NSURL(string: urlPath)!
        
        // Set request variables
        let request = NSMutableURLRequest(URL: url)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        let coordinate = FoodTruckTrackerDatastore.userLocation.coordinate
        let dictionary = ["loc_latitude": coordinate.latitude, "loc_longitude": coordinate.longitude]
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(dictionary, options: [])
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // Start the data task
        let task : NSURLSessionDataTask = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            print(response)
            if((error) != nil){
                print("Error getting food trucks - " + error!.description)
                
            }else{
                var events: NSMutableArray?
                do {
                    let jsonEvents = try NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.AllowFragments) as! NSArray
                    
                    print(jsonEvents)
                    events = TruckTrackerServiceCallParser.parseGetFoodTrucks(jsonEvents)
                    
                } catch {
                    print("json serialization error: %@", error)
                }
                
                dispatch_async(dispatch_get_main_queue(),{
                    completion(result: events)
                })
            }
        });
        task.resume()
    }
    
    /**
     Adds a new food truck sighting
    **/
    static func addFoodTruckSighting(foodTruckName: String, location: CLLocationCoordinate2D, completion: () -> Void) {
        print("Adding food truck")
        let urlPath: String = "http://fierce-dawn-82229.herokuapp.com/reporttrucksighting"
        
        let loginString = NSString(format: "%@:%@", FoodTruckTrackerDatastore.email()!, FoodTruckTrackerDatastore.appPassword()!)
        print("Authorization: " + (loginString as String))
        
        let userPasswordData = loginString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = userPasswordData!.base64EncodedStringWithOptions([])
        let authString = "Basic \(base64EncodedCredential)"
        
        let url: NSURL = NSURL(string: urlPath)!
        
        // Set request variables
        let request = NSMutableURLRequest(URL: url)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        request.setValue(authString, forHTTPHeaderField: "Authorization")

        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        print(dateFormatter.stringFromDate(NSDate()))
        
        let dictionary = ["truckname": foodTruckName,
        "truckhours": "truckhours",
        "loc_latitude": location.latitude,
        "loc_longitude" : location.longitude,
        "loc_state": "IL",
        "loc_city": "Champaign",
        "loc_zip": "61820",
        "loc_address": "123",
        "loc_country": "US",
        "sight_rating": 5.0,
        "sight_description": foodTruckName,
        "sight_time":  dateFormatter.stringFromDate(NSDate()),
        "user_sk": 1.0]
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(dictionary, options: [])
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // Start the data task
        let task : NSURLSessionDataTask = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            if((error) != nil){
                print(response)
                print("Error reporting food truck - " + error!.description)
                
            }else{
                do {
                    print(response)
                    
                } catch {
                    print("json serialization error: %@", error)
                }
                
                dispatch_async(dispatch_get_main_queue(),{
                    completion()
                })
            }
        });
        task.resume()
    }
    
    static func createUser(email: String, fbToken: String, completion: () -> Void) {
        print(String(format:"Adding user\nusername: %@\npassword: %@\n", email, fbToken))
        let urlPath: String = "http://fierce-dawn-82229.herokuapp.com/users"
        let url: NSURL = NSURL(string: urlPath)!
        
        // Set request variables
        let request = NSMutableURLRequest(URL: url)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        
        let dictionary = ["username": email,
                          "password": FoodTruckTrackerDatastore.appPassword()!,
                          "user_email": email]
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(dictionary, options: [])
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // Start the data task
        let task : NSURLSessionDataTask = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            if((error) != nil){
                print(response)
                print("Error adding user - " + error!.description)
                
            }else{
                do {
                    print(response)
                    
                } catch {
                    print("json serialization error: %@", error)
                }
                
                dispatch_async(dispatch_get_main_queue(),{
                    completion()
                })
            }
        });
        task.resume()
    }
    
    // MARK: Company calls
    
    static func createCompany(name: String, phone: String, email: String, city: String, zipcode: String, state: String, completion: (companySK: String) -> Void) {
        print("Adding company")
        let urlPath: String = "http://fierce-dawn-82229.herokuapp.com/newcompany"
        let url: NSURL = NSURL(string: urlPath)!
        
        // Set request variables
        let request = NSMutableURLRequest(URL: url)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        
        let dictionary = ["loc_latitude": 0.0,
                          "loc_longitude": 0.0,
                          "loc_state": state,
                          "loc_city": city,
                          "loc_zip": zipcode,
                          "loc_address": "address",
                          "loc_country": "USA",
                          "company_name": name,
                          "company_phone": phone]
        
        let loginString = NSString(format: "%@:%@", FoodTruckTrackerDatastore.email()!, FoodTruckTrackerDatastore.appPassword()!)
        print("Authorization: " + (loginString as String))
        
        let userPasswordData = loginString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = userPasswordData!.base64EncodedStringWithOptions([])
        let authString = "Basic \(base64EncodedCredential)"
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(dictionary, options: [])
        request.setValue(authString, forHTTPHeaderField: "Authorization")

        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // Start the data task
        let task : NSURLSessionDataTask = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            if((error) != nil){
                print(response)
                print("Error adding company - " + error!.description)
                
            }else{
                var companySK: String = ""
                do {
                    print(response)
                    let jsonEvents = try NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.AllowFragments) as! NSArray
                    let jsonDictionary = jsonEvents.objectAtIndex(0)
                    print(jsonEvents)
                    companySK = (jsonDictionary.objectForKey("company_sk") as! NSNumber).stringValue
                    
                } catch {
                    print("json serialization error: %@", error)
                }
                
                dispatch_async(dispatch_get_main_queue(),{
                    completion(companySK: companySK)
                })
            }
        });
        task.resume()
    }
    
    /**
     Adds a new food truck for company
     **/
    static func addFoodTruckForCompany(foodTruckName: String, locationName: String, location: CLLocationCoordinate2D, completion: () -> Void) {
        print("Adding food truck for company")
        let urlPath: String = "http://fierce-dawn-82229.herokuapp.com/ownerreporttruck"
        
        let loginString = NSString(format: "%@:%@", FoodTruckTrackerDatastore.email()!, FoodTruckTrackerDatastore.appPassword()!)
        print("Authorization: " + (loginString as String))
        
        let userPasswordData = loginString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = userPasswordData!.base64EncodedStringWithOptions([])
        let authString = "Basic \(base64EncodedCredential)"
        
        let url: NSURL = NSURL(string: urlPath)!
        
        // Set request variables
        let request = NSMutableURLRequest(URL: url)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        request.setValue(authString, forHTTPHeaderField: "Authorization")
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        print(dateFormatter.stringFromDate(NSDate()))
        
        let dictionary = ["truckname": foodTruckName,
                          "truckhours": "truckhours",
                          "loc_latitude": location.latitude,
                          "loc_longitude" : location.longitude,
                          "loc_state": "IL",
                          "loc_city": "Champaign",
                          "loc_zip": "61820",
                          "loc_address": "123",
                          "loc_country": "US",
                          "sight_rating": "5.0",
                          "sight_description": locationName,
                          "sight_time":  dateFormatter.stringFromDate(NSDate()),
                          "company_sk": FoodTruckTrackerDatastore.companySK()!]
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(dictionary, options: [])
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // Start the data task
        let task : NSURLSessionDataTask = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            if((error) != nil){
                print(response)
                print("Error reporting food truck - " + error!.description)
                
            }else{
                do {
                    print(response)
                    
                } catch {
                    print("json serialization error: %@", error)
                }
                
                dispatch_async(dispatch_get_main_queue(),{
                    completion()
                })
            }
        });
        task.resume()
    }
    
    /**
     Get all of the companies food trucks
     **/
    static func getCompanyFoodTrucks(companySK: String, completion: (result: NSMutableArray?) -> Void) {
        print("Getting company food trucks")
        let urlPath: String = "http://fierce-dawn-82229.herokuapp.com/companytrucks/" + companySK
        let url: NSURL = NSURL(string: urlPath)!
        
        // Set request variables
        let request = NSMutableURLRequest(URL: url)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "GET"
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        
        // Start the data task
        let task : NSURLSessionDataTask = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            print(response)
            if((error) != nil){
                print("Error getting company food trucks - " + error!.description)
                
            }else{
                var trucks: NSMutableArray?
                do {
                    let jsonEvents = try NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.AllowFragments) as! NSArray
                    print(jsonEvents)
                    trucks = TruckTrackerServiceCallParser.parseGetCompanyFoodTrucks(jsonEvents)
                    
                } catch {
                    print("json serialization error: %@", error)
                }
                
                dispatch_async(dispatch_get_main_queue(),{
                    completion(result: trucks)
                })
            }
        });
        task.resume()
    }
}


// Test data

/*
let foodTrucks = NSMutableArray()

let truck1: FoodTruckTrackerFoodTruck = FoodTruckTrackerFoodTruck()
truck1.location = CLLocation(latitude:40.110131, longitude:-88.225550)
truck1.name = "Cracked"
truck1.street = "Mathews & Springfield"
truck1.phoneNumber = "8475423161"
truck1.menu = "Cracked-Menu-2015"

let truck2: FoodTruckTrackerFoodTruck = FoodTruckTrackerFoodTruck()
truck2.location = CLLocation(latitude:40.110303, longitude:-88.227266)
truck2.name = "Truck 2"
truck2.street = "Fake St."
truck2 .phoneNumber = "8475423161"
truck2.menu = "Cracked-Menu-2015"

let truck3: FoodTruckTrackerFoodTruck = FoodTruckTrackerFoodTruck()
truck3.location = CLLocation(latitude:40.109557, longitude:-88.223844)
truck3.name = "Truck 3"
truck3.street = "Fake St."
truck3.phoneNumber = "8475423161"
truck3.menu = "Cracked-Menu-2015"

let truck4: FoodTruckTrackerFoodTruck = FoodTruckTrackerFoodTruck()
truck4.location = CLLocation(latitude:40.110394, longitude:-88.223104)
truck4.name = "Truck 4"
truck4.street = "Fake St."
truck4.phoneNumber = "8475423161"
truck4.menu = "Cracked-Menu-2015"

let truck5: FoodTruckTrackerFoodTruck = FoodTruckTrackerFoodTruck()
truck5.location = CLLocation(latitude:40.109926, longitude:-88.228886)
truck5.name = "Truck 5"
truck5.street = "Fake St."
truck5.phoneNumber = "8475423161"
truck5.menu = "Cracked-Menu-2015"

foodTrucks.addObject(truck1)
foodTrucks.addObject(truck2)
foodTrucks.addObject(truck3)
foodTrucks.addObject(truck4)
foodTrucks.addObject(truck5)

return foodTrucks
*/
