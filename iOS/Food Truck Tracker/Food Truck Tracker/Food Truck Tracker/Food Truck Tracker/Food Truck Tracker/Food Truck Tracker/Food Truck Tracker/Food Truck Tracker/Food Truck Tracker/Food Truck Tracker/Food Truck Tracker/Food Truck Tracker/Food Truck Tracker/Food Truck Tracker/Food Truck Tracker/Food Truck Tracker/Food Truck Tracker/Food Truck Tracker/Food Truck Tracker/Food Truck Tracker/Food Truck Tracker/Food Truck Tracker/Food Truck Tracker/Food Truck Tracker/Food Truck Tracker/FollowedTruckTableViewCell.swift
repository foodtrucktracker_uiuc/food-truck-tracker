//
//  FollowedTruckTableViewCell.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 11/26/15.
//  Copyright © 2015 James Wegner. All rights reserved.
//

import UIKit

class FollowedTruckTableViewCell: UITableViewCell {
    @IBOutlet weak var truckNameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
