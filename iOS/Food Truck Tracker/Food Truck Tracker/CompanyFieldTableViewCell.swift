//
//  CompanyFieldTableViewCell.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 4/12/16.
//  Copyright © 2016 James Wegner. All rights reserved.
//

import UIKit

class CompanyFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var infoTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
