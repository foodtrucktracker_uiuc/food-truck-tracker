//
//  FoodTruckTableViewCell.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 11/17/15.
//  Copyright © 2015 James Wegner. All rights reserved.
//

import UIKit

class FoodTruckTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var truckLogoView: UIImageView!
    
    
    @IBOutlet weak var truckNameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
