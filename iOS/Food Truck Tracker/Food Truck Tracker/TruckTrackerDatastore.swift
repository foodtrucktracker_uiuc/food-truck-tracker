//
//  FoodTruckTrackerDatastore.swift
//  Food Truck Tracker
//
//  Created by James Wegner on 11/11/15.
//  Copyright © 2015 James Wegner. All rights reserved.
//

import UIKit
import CoreLocation

class FoodTruckTrackerDatastore: NSObject {
    static var userLocation: CLLocation = CLLocation()
    static var datastore = NSUserDefaults.standardUserDefaults()
    
    static func setEmail(email: String) {
        datastore.setValue(email, forKey: "EMAIL")
    }
    
    static func email() -> String? {
        return "jweg293@gmail.com"
        //return datastore.valueForKey("EMAIL") as! String?
    }
    
    static func setFacebookToken(token: String) {
        datastore.setValue(token, forKey: "FACEBOOK_TOKEN")
    }
    
    static func facebookToken() -> String? {
        return datastore.valueForKey("FACEBOOK_TOKEN") as! String?
    }
    
    static func userIsLoggedIn() -> Bool {
        return datastore.valueForKey("LOGGED_IN") as! Bool
    }
    
    static func setUserLoggedIn(loggedIn: Bool) {
        datastore.setValue(loggedIn, forKey: "LOGGED_IN")
    }
    
    static func setCompanySK(companySK: String) {
        datastore.setValue(companySK, forKey: "COMPANY_SK")
    }
    
    static func companySK() -> String? {
        return datastore.valueForKey("COMPANY_SK") as! String?
    }
    
    static func appPassword() -> String? {
        return "password"
    }
}
